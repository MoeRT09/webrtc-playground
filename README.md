# WebRTC Playground

WebRTC Playground is an Angular based web application providing peer-to-peer real-time communication using WebRTC. It is mainly intended for evaluating and experimenting with the WebRTC implementations of web browsers.

However, it provides advanced customizability and WebRTC statistics logging options in order to experiment with WebRTC as a technology.

## Features
- Basic video conferencing, by multiple clients joining the same room;
- Stream a video / audio file from your hard drive, instead if your microphone / webcam;
- Specify the codec to use for sending / receiving audio / video;
- Specify advanced, codec specific options by defining the SDP `fmtp` line;
- Specify the (maximum) bitrate to use for sending / receiving audio / video;
- Send out REST requests to an endpoint, as soon as a user joins / leaves a room (currently no HTTP header / body can be specified);
- Specify filters for ICE-candidates;
- View the WebRTC statistics of a client's peer connection in real time;
- Record the WebRTC statistics of a client's peer connection and download it as a JSON file.

## How to get started
### Prerequisites
WebRTC Playground uses the [Open-EasyRTC](https://github.com/open-easyrtc/open-easyrtc) library for signalling. Therefore, it is necessary to have their signalling server setup and configured. See the [Open-EasyRTC repository](https://github.com/open-easyrtc/open-easyrtc) for details. Once properly running, enter the URL, under which the server is reachable in the environment files under `src/environments`.

Furthermore, you need [node.js](https://nodejs.org/) installed.

### Running a development instance
Start the development server by running `npm run start`. The application will be available under [http://localhost:4200/](http://localhost:4200/) by default.

### Creating a production build
Run `npm run build --prod`. This will generate static HTML and JavaScript files in the `dist` directory. The contents of the `dist` directory can be deployed to a web server. The production build will use the socket server configured in the `src/environments/environment.prod.ts` file.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.2.

## Screenshots
**Start page for joining a room:**

![Start page for joining a room](screenshots/join-room.png "Start page for joining a room")


**Room with four connected clients, streaming the trailes of ['Sintel'](https://durian.blender.org/) and ['Big Buck Bunny'](https://peach.blender.org/) (© copyright Blender Foundation):**

![Room fith four connected clients, streaming the trailes of 'Sintel' and 'Big Buck Bunny'](screenshots/room.png "Room fith four connected clients, streaming the trailes of 'Sintel' and 'Big Buck Bunny'")


**Real-time view of WebRTC statistics:**

![Real-time view of WebRTC statistics](screenshots/stats3.png "Real-time view of WebRTC statistics")


**Display the session descriptions in use:**

![Display the session descriptions in use](screenshots/stats-sdp.png "Display the session descriptions in use")


**Raw view of the browser's supported statistics:**

![Raw view of the browser's supported statistics](screenshots/raw-stats.png "Raw view of the browser's supported statistics")


**Options for outbound audio / video:**

![Options for outbound audio / video](screenshots/settings-media.png "Options for outbound audio / video")


**Codec / bitrate options:**

![Codec / bitrate options](screenshots/settings-codec-bitrate-extended.png "Codec / bitrate options")


**Settings for triggering REST-API endpoints:**

![Settings for triggering REST-API endpoints](screenshots/settings-automation.png "Settings for triggering REST-API endpoints")


**Settings for specifying ICE-Candidate filters:**

![Settings for specifying ICE-Candidate filters](screenshots/settings-filter.png "Settings for specifying ICE-Candidate filters")