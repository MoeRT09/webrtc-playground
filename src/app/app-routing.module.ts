import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JoinRoomComponent } from './join-room/join-room.component';
import { RoomComponent } from './room/room.component';

const routes: Routes = [
  { path: '', component: JoinRoomComponent, pathMatch: 'full'},
  { path: 'room/:roomId/:userName', component: RoomComponent },
  { path: 'room/:roomId', component: JoinRoomComponent },
  { path: '**',   redirectTo: '', pathMatch: 'full' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
