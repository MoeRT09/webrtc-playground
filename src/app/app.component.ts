import { Component, OnInit, OnDestroy, HostBinding, Renderer2 } from '@angular/core';
import { Event } from './shared/model/event.enum';
import { MatSnackBar } from '@angular/material/snack-bar';
import { WebsocketConnectionSnackbarComponent } from './shared/websocket-connection-snackbar/websocket-connection-snackbar.component';
import { EasyRtcService } from './shared/services/easy-rtc.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { SettingsComponent } from './dialogs/settings/settings.component';
import { ApiLogComponent } from './dialogs/api-log/api-log.component';
import { ApiInteractorService } from './shared/services/api-interactor.service';
import { AboutComponent } from './dialogs/about/about.component';
import { ThemingService } from './shared/services/theming.service';

@Component({
  selector: 'wp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'WebRTC Playground';

  constructor(
    private wsSnackBar: MatSnackBar,
    private rtc: EasyRtcService,
    public router: Router,
    private dialog: MatDialog,
    public apiInteraction: ApiInteractorService,
    private themingService: ThemingService,
    private renderer: Renderer2
  ) {}

  private subscriptions = new Array<Subscription>();

  ngOnInit() {
    const themeServiceSubscription = this.themingService.theme.subscribe((newTheme: string) => {
      this.renderer.addClass(document.body, newTheme);
      this.themingService.themes
        .filter((theme) => theme !== newTheme)
        .forEach((oldTheme) => {
          this.renderer.removeClass(document.body, oldTheme);
        });
    });

    this.subscriptions.push(themeServiceSubscription);

    this.initGlobalSocketEventHandlers();
    // using setTimout because of a bug. See https://github.com/angular/angular/issues/17572
    setTimeout(() => {
      if (!this.rtc.isConnectedToSocket) {
        this.showDisconnectSnackBar();
      }
    }, 500);
  }

  private showDisconnectSnackBar(): void {
    this.wsSnackBar.openFromComponent(WebsocketConnectionSnackbarComponent, {
      data: {
        webSocketServer: this.rtc.socketServerUrl,
      },
    });
  }

  private initGlobalSocketEventHandlers(): void {
    const connectSub = this.rtc.onEvent(Event.CONNECT).subscribe((reason) => {
      console.log('connected: ', reason);
      this.wsSnackBar.dismiss();
    });

    const disconnectSub = this.rtc.onEvent(Event.DISCONNECT).subscribe((reason) => {
      console.log('disconnected: ', reason);
      if (reason === 'io client disconnect') {
        this.rtc.connectSocket();
      }
      this.showDisconnectSnackBar();
    });

    this.subscriptions.push(connectSub, disconnectSub);
  }

  public showSettings() {
    this.dialog.open(SettingsComponent, {
      minWidth: '60%',
      maxWidth: '95vw',
    });
  }

  public showAbout() {
    this.dialog.open(AboutComponent, {
      width: '600px',
      maxWidth: '95vw',
      data: this.title,
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
