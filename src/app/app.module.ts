import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from './shared/shared.module';
import { AppComponent } from './app.component';
import { JoinRoomComponent } from './join-room/join-room.component';
import { RoomComponent } from './room/room.component';
import { EasyRtcService } from './shared/services/easy-rtc.service';
import { ParticipantsAreaComponent } from './room/participants-area/participants-area.component';
import { ParticipantsChipsComponent } from './room/participants-chips/participants-chips.component';
import { ParticipantsListComponent } from './room/participants-list/participants-list.component';
import { ParticipantMuteComponent } from './room/participant-mute/participant-mute.component';
import { UserIconComponent } from './room/user-icon/user-icon.component';
import { UserInfoComponent } from './dialogs/user-info/user-info.component';
import { MatDynamicTreeComponent } from './shared/mat-dynamic-tree/mat-dynamic-tree.component';
import { PeerStatsComponent } from './shared/peer-stats/peer-stats.component';
import { RawStatsComponent } from './dialogs/raw-stats/raw-stats.component';
import { SettingsComponent } from './dialogs/settings/settings.component';
import { FileStreamControlsComponent } from './room/file-stream-controls/file-stream-controls.component';
import { RecordStatsControlsComponent } from './room/record-stats-controls/record-stats-controls.component';
import { CodecSelectComponent } from './dialogs/settings/codec-select/codec-select.component';
import { ImportSettingsComponent } from './dialogs/settings/import-settings/import-settings.component';
import { ApiLogComponent } from './dialogs/api-log/api-log.component';
import { AboutComponent } from './dialogs/about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    JoinRoomComponent,
    RoomComponent,
    ParticipantsAreaComponent,
    ParticipantsChipsComponent,
    ParticipantsListComponent,
    ParticipantMuteComponent,
    UserIconComponent,
    UserInfoComponent,
    MatDynamicTreeComponent,
    PeerStatsComponent,
    RawStatsComponent,
    SettingsComponent,
    FileStreamControlsComponent,
    RecordStatsControlsComponent,
    CodecSelectComponent,
    ImportSettingsComponent,
    ApiLogComponent,
    AboutComponent,
  ],
  imports: [BrowserModule, HttpClientModule, BrowserAnimationsModule, AppRoutingModule, SharedModule, FormsModule, FlexLayoutModule],
  providers: [EasyRtcService],
  bootstrap: [AppComponent],
})
export class AppModule {}
