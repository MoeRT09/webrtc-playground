import { Component, OnInit, Inject, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ApiInteractorService } from '../../shared/services/api-interactor.service';
import { ApiInteractionLog } from '../../shared/model/api-interaction-log';
import { Subscription } from 'rxjs';

@Component({
  selector: 'wp-api-log',
  templateUrl: './api-log.component.html',
  styleUrls: ['api-log.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApiLogComponent implements OnInit, OnDestroy {
  constructor(public apiInteraction: ApiInteractorService, private cdRef: ChangeDetectorRef) {}

  public typesToSkip = new RegExp('Function|_w+');
  public propertiesToSkip = new RegExp(/(lazyUpdate|lazyInit)/);
  private logSub: Subscription;
  ngOnInit(): void {
    this.logSub = this.apiInteraction.apiLog$.subscribe(this.onLog.bind(this));
  }

  private onLog(message: ApiInteractionLog) {
    this.cdRef.detectChanges();
  }

  ngOnDestroy(): void {
    this.logSub.unsubscribe();
  }
}
