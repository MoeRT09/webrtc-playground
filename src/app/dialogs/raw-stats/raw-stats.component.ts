import { Component, OnInit, Inject, ViewChild, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Util } from '../../shared/util';
import { MatDynamicTreeComponent } from '../../shared/mat-dynamic-tree/mat-dynamic-tree.component';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'wp-raw-stats',
  templateUrl: './raw-stats.component.html',
  styles: [],
})
export class RawStatsComponent implements OnInit, OnDestroy {
  @ViewChild('treeComponent') treeComponent: MatDynamicTreeComponent;
  constructor(@Inject(MAT_DIALOG_DATA) public statsReportObservable: Observable<Map<string, RTCStats>>) {}

  public statsReport: Map<string, RTCStats>;
  private statsReportSubscription: Subscription;
  public get statsReportObject() {
    if (this.statsReport) {
      const reportObject = Util.mapToObject(this.statsReport);
      return reportObject;
    }
    return {};
  }

  ngOnInit(): void {
    this.statsReportSubscription = this.statsReportObservable.subscribe((statsReport) => {
      this.statsReport = statsReport;
    });
  }

  ngOnDestroy(): void {
    this.statsReportSubscription.unsubscribe();
  }
}
