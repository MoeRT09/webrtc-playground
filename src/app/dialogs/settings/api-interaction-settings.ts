import { ApiInteraction } from "../../shared/services/api-interactor.service";

export interface ApiInteractionSettings extends ApiInteraction{
  id: number;
  executeOnFirst?: boolean;
  executeOnLast?: boolean;
}
