import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodecSelectComponent } from './codec-select.component';

describe('CodecSelectComponent', () => {
  let component: CodecSelectComponent;
  let fixture: ComponentFixture<CodecSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodecSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodecSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
