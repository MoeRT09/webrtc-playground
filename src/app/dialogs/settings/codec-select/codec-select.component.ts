
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CodecConfig, RtcCodecs } from '../../../shared/rtc-media-settings/rtc-codecs';


@Component({
  selector: 'wp-codec-select',
  templateUrl: './codec-select.component.html',
  styleUrls: ['./codec-select.component.css']
})
export class CodecSelectComponent implements OnInit {
  @Input() settingToBind: CodecConfig;
  @Output() settingToBindChange: EventEmitter<CodecConfig> = new EventEmitter<CodecConfig>();
  @Input() availableCodecs: CodecConfig[];
  @Input() label: string;
  constructor() { }

  ngOnInit(): void {
  }

  public codecCapabilitiesToString(capability: RTCRtpCodecCapability): string {
    const caps = new Array<string>();

    if (capability.channels) {
      caps.push(`Channels: ${capability.channels}`);
    }

    if (capability.clockRate) {
      caps.push(`Clockrate: ${capability.clockRate}`);
    }

    if (capability.sdpFmtpLine) {
      caps.push(`FMTP: "${capability.sdpFmtpLine}"`);
    }

    return caps.join('; ');
  }

  public codecConfigEquals(config1: CodecConfig, config2: CodecConfig) {
    return RtcCodecs.codecCapabilitiesEquals(config1?.codecCapability, config2?.codecCapability);
  }

}
