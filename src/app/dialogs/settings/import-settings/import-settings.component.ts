import { Component, OnInit, Inject } from '@angular/core';
import { RtcSettings } from '../../../shared/model/rtc-settings';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'wp-import-settings',
  templateUrl: './import-settings.component.html',
  styleUrls: ['./import-settings.component.css'],
})
export class ImportSettingsComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: RtcSettings) {}

  public settingsString: string;

  public isValidSettings(): boolean {
    try {
      const parsed = this.parsedSettings();
      if (typeof parsed === 'object') {
        return true;
      }
    } catch {}
    return false;
  }

  public parsedSettings(): RtcSettings {
    try {
      const parsed = JSON.parse(this.settingsString);
      return parsed;
    } catch {
      return undefined;
    }
  }

  ngOnInit(): void {}
}
