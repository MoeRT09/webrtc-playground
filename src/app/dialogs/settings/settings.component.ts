import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { RtcSettings } from '../../shared/model/rtc-settings';
import { RtcSettingsService } from '../../shared/services/rtc-settings.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { HttpMethod, ApiInteractorService } from '../../shared/services/api-interactor.service';
import { ApiInteractionSettings } from './api-interaction-settings';
import { Clipboard } from '@angular/cdk/clipboard';
import { ImportSettingsComponent } from './import-settings/import-settings.component';
import { first } from 'rxjs/operators';

@Component({
  selector: 'wp-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
})
export class SettingsComponent implements OnInit {
  public currentSettings: RtcSettings = {};
  @ViewChild('fileInput') fileInput: ElementRef<HTMLInputElement>;

  onClickFileInputButton(): void {
    this.fileInput.nativeElement.click();
  }

  constructor(
    public settings: RtcSettingsService,
    private dialogRef: MatDialogRef<SettingsComponent>,
    private clipboard: Clipboard,
    private dialog: MatDialog,
    private apiInteractor: ApiInteractorService
  ) {}

  public saveSettings() {
    this.settings.settings = { ...this.currentSettings };
    if (this.currentSettings.preferredSendAudioCodec)
      this.settings.settings.preferredSendAudioCodec = { ...this.currentSettings.preferredSendAudioCodec };
    if (this.currentSettings.preferredSendVideoCodec)
      this.settings.settings.preferredSendVideoCodec = { ...this.currentSettings.preferredSendVideoCodec };
    if (this.currentSettings.preferredReceiveAudioCodec)
      this.settings.settings.preferredReceiveAudioCodec = { ...this.currentSettings.preferredReceiveAudioCodec };
    if (this.currentSettings.preferredReceiveVideoCodec)
      this.settings.settings.preferredReceiveVideoCodec = { ...this.currentSettings.preferredReceiveVideoCodec };
    if (this.currentSettings.apiInteractions) {
      this.settings.settings.apiInteractions = [];
      this.currentSettings.apiInteractions.forEach((interaction) => this.settings.settings.apiInteractions.push({ ...interaction }));
    }
  }

  public applySettings() {
    this.saveSettings();
    this.settings.applySettings();
    this.dialogRef.close();
  }

  private loadCurrentSettings(): void {
    this.currentSettings = { ...this.settings.settings };
    if (this.settings.settings.preferredSendAudioCodec)
      this.currentSettings.preferredSendAudioCodec = { ...this.settings.settings.preferredSendAudioCodec };
    if (this.settings.settings.preferredSendVideoCodec)
      this.currentSettings.preferredSendVideoCodec = { ...this.settings.settings.preferredSendVideoCodec };
    if (this.settings.settings.preferredReceiveAudioCodec)
      this.currentSettings.preferredReceiveAudioCodec = { ...this.settings.settings.preferredReceiveAudioCodec };
    if (this.settings.settings.preferredReceiveVideoCodec)
      this.currentSettings.preferredReceiveVideoCodec = { ...this.settings.settings.preferredReceiveVideoCodec };
    if (this.settings.settings.apiInteractions) {
      this.currentSettings.apiInteractions = [];
      this.settings.settings.apiInteractions.forEach((interaction) => this.currentSettings.apiInteractions.push({ ...interaction }));
    }
  }

  public getHttpMethods() {
    return Object.keys(HttpMethod);
  }

  public removeApiInteraction(index: number): void {
    this.currentSettings?.apiInteractions?.splice(index, 1);
  }

  public addApiInteraction(): void {
    if (!this.currentSettings.apiInteractions) {
      this.currentSettings.apiInteractions = new Array<ApiInteractionSettings>();
    }
    this.currentSettings.apiInteractions.push({
      id: Date.now(),
      endpointUrl: '',
      method: HttpMethod.get,
    });
  }

  public copySettingsString() {
    const settings = JSON.stringify(this.currentSettings);
    this.clipboard.copy(settings);
  }

  public loadSettingsString() {
    const dialogRef = this.dialog.open(ImportSettingsComponent, {
      width: '500px',
      maxWidth: '95vw'
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.currentSettings = result;
      }
    });
  }

  public triggerInteraction(interaction: ApiInteractionSettings) {
    this.apiInteractor.sendRequest(interaction).pipe(first()).subscribe();
  }

  ngOnInit(): void {
    this.loadCurrentSettings();
    this.settings.updateSupportedCodecs();
    console.log(this.settings);
  }
}
