import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { User } from '../../shared/model/user';
import { SdpRendererComponent } from '../../shared/peer-stats/sdp-renderer/sdp-renderer.component';

@Component({
  selector: 'wp-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css'],
})
export class UserInfoComponent implements OnInit {
  public printPrototypeFor = [
    'MediaStream',
    'MediaStreamTrack',
    'RTCPeerConnection',
    'RTCSessionDescription',
    'RTCRtpSender',
    'RTCDtlsTransport',
    'RTCDTMFSender',
    'RTCIceTransport',
    'RTCIceCandidate',
  ];
  public typesToSkip = new RegExp('Function|_w+');
  public propertiesToSkip = new RegExp(/(?!_shimmedLocalStreams|_peerconnection)_\w+/);
  public functionsToExpand = ['getTracks'];

  constructor(@Inject(MAT_DIALOG_DATA) public user: User) {}

  ngOnInit(): void {}

  public propertyFormatter(key: string, value: string): string {
    if (key === 'sdp') {
      value = SdpRendererComponent.sessionDescriptionFormatter(value);
    }
    return value;
  }

  public getSelectedIceCandidates() {
    try {
      return this.user.rtcStats.getUniqueSelectedIceCandidatePairs();
    } catch (e) {
      return { Error: e.message };
    }
  }


}
