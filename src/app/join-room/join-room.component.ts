import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

import { EasyRtcService } from '../shared/services/easy-rtc.service';

@Component({
  selector: 'wp-join-room',
  templateUrl: './join-room.component.html',
  styles: [],
})
export class JoinRoomComponent implements OnInit {
  @ViewChild('joinRoomForm', { static: true }) joinRoomForm: NgForm;

  constructor(private route: ActivatedRoute, private router: Router, public rtc: EasyRtcService) {}

  public roomNameRegexp: string = '^[a-zA-Z0-9_.-]{1,32}$';
  public userNameRegexp: string = '^[^/\\\\#?"&\\n]{1,32}$';

  roomId: string = '';
  userName: string = '';

  ngOnInit() {
    const preselectedRoom = this.route.snapshot.params['roomId'];
    if (preselectedRoom) {
      this.roomId = preselectedRoom;
    }
  }

  public async joinRoom(value: any) {
    this.userName = value.userName;
    this.roomId = value.roomId;
    console.log(this.roomId, this.userName);
    if (this.joinRoomForm.valid) {
      await this.rtc.joinRoom(this.roomId, this.userName);
      const navigationExtras: NavigationExtras = {
        state: {
          fromRoomJoin: true,
        },
      };
      this.router.navigate(['/room', this.roomId, this.userName], navigationExtras);
    }
  }
}
