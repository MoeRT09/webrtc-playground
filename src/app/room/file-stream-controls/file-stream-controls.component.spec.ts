import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileStreamControlsComponent } from './file-stream-controls.component';

describe('FileStreamControlsComponent', () => {
  let component: FileStreamControlsComponent;
  let fixture: ComponentFixture<FileStreamControlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileStreamControlsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileStreamControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
