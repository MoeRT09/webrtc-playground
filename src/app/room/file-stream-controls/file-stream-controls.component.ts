import { Component, OnInit, Input } from '@angular/core';
import { EasyRtcService } from '../../shared/services/easy-rtc.service';
import { User } from '../../shared/model/user';

@Component({
  selector: 'wp-file-stream-controls',
  templateUrl: './file-stream-controls.component.html',
  styleUrls: ['./file-stream-controls.component.css']
})
export class FileStreamControlsComponent implements OnInit {

  constructor(public rtc: EasyRtcService) { }
  @Input() user: User;

  ngOnInit(): void {
    console.log(this.user);
  }

  public togglePlay() {
    if(this.rtc.isFileStreamPaused()) {
      this.rtc.playFileStream();
    }
    else {
      this.rtc.pauseFileStream();
    }
  }

}
