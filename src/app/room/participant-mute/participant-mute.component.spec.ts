import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantMuteComponent } from './participant-mute.component';

describe('ParticipantMuteComponent', () => {
  let component: ParticipantMuteComponent;
  let fixture: ComponentFixture<ParticipantMuteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantMuteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantMuteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
