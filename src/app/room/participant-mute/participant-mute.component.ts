import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../shared/model/user';

@Component({
  selector: 'wp-participant-mute',
  templateUrl: './participant-mute.component.html',
  styles: [
  ]
})
export class ParticipantMuteComponent implements OnInit {
  @Input() user: User;
  constructor() { }

  ngOnInit(): void {
  }

}
