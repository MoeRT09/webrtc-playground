import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantsAreaComponent } from './participants-area.component';

describe('ParticipantsAreaComponent', () => {
  let component: ParticipantsAreaComponent;
  let fixture: ComponentFixture<ParticipantsAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantsAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantsAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
