import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../shared/model/user';
import { EasyRtcService } from '../../shared/services/easy-rtc.service';

@Component({
  selector: 'wp-participants-area',
  templateUrl: './participants-area.component.html',
  styleUrls: ['./participants-area.component.css']
})
export class ParticipantsAreaComponent implements OnInit {
  @Input() participants: Array<User>
  constructor(public rtc: EasyRtcService) { }

  ngOnInit(): void {
  }

  public colCount(amount: number): number {
    return Math.ceil(Math.sqrt(amount));
  }

  public participantsWithVideo(): number {
    let count = 0;
    
    this.participants.forEach(user => {
      if (user.hasVideoTracks()) {
        count++;
      }
    });
    return count;
  }
}
