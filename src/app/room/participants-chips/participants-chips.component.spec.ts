import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantsChipsComponent } from './participants-chips.component';

describe('ParticipantsListComponent', () => {
  let component: ParticipantsChipsComponent;
  let fixture: ComponentFixture<ParticipantsChipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantsChipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantsChipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
