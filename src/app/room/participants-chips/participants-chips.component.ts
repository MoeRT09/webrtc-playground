import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../shared/model/user';

@Component({
  selector: 'wp-participants-chips',
  templateUrl: './participants-chips.component.html',
  styleUrls: ['./participants-chips.component.css'],
})
export class ParticipantsChipsComponent implements OnInit {
  @Input() participants: Array<User>;
  @Input() isConnected: boolean;
  constructor() {}

  ngOnInit(): void {}


}
