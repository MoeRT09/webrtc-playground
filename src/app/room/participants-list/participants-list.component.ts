import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../shared/model/user';
import { MatDialog } from '@angular/material/dialog';
import { UserInfoComponent } from '../../dialogs/user-info/user-info.component';

@Component({
  selector: 'wp-participants-list',
  templateUrl: './participants-list.component.html',
  styleUrls: ['participants-list.component.css'],
})
export class ParticipantsListComponent implements OnInit {
  @Input() participants: Array<User>;
  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  public showUserInfo(user: User): void {
    this.dialog.open(UserInfoComponent, {
      data: user,
      minWidth: '60%',
      maxWidth: '95vw'
    });
  }
}
