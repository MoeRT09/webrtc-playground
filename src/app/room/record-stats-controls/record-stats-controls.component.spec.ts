import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordStatsControlsComponent } from './record-stats-controls.component';

describe('RecordStatsControlsComponent', () => {
  let component: RecordStatsControlsComponent;
  let fixture: ComponentFixture<RecordStatsControlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecordStatsControlsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordStatsControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
