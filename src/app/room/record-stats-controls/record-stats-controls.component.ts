import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { User } from '../../shared/model/user';
import { RtcSettingsService } from '../../shared/services/rtc-settings.service';

@Component({
  selector: 'wp-record-stats-controls',
  templateUrl: './record-stats-controls.component.html',
  styles: [],
})
export class RecordStatsControlsComponent implements OnInit, OnDestroy {
  @Input() user: User;
  constructor(private settings: RtcSettingsService) {}

  ngOnDestroy(): void {
    if (this?.user?.rtcStats?.statRecorder?.isRecording) {
      this?.user?.rtcStats?.statRecorder?.stop();
      this.downloadStats();
    }
  }

  ngOnInit(): void {}

  public toggleRecord() {
    if (this?.user?.rtcStats?.statRecorder?.isRecording) {
      this?.user?.rtcStats?.statRecorder?.stop();
    } else {
      this?.user?.rtcStats?.statRecorder?.start();
    }
  }

  public downloadStats() {
    const shadowLink = document.createElement('a');
    shadowLink.setAttribute('href', this.generateDownloadJsonUri(this?.user?.rtcStats?.statRecorder?.recordedStats));
    shadowLink.setAttribute('download', `SummaryStats_${this.user.userName}_(${this.user.easyrtcid}).json`);
    shadowLink.style.display = 'none';
    document.body.appendChild(shadowLink);
    shadowLink.click();
    document.body.removeChild(shadowLink);
  }

  private generateDownloadJsonUri(obj: Object) {
    let theJSON = JSON.stringify(obj, this.statObjectReplacer.bind(this), 2);
    let blob = new Blob([theJSON], { type: 'text/json' });
    return window.URL.createObjectURL(blob);
  }

  private statObjectReplacer(key: string, value: any) {
    if (!this.settings.settings?.includeSdpInStatRecording && (key === 'localSessionDescription' || key === 'remoteSessionDescription')) {
      return undefined;
    }

    return value;
  }
}
