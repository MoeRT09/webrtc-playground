import { Component, OnDestroy, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EasyRtcService } from '../shared/services/easy-rtc.service';
import { Subscription, Observable } from 'rxjs';
import { take, filter } from 'rxjs/operators';
import { RtcSettingsService } from '../shared/services/rtc-settings.service';

@Component({
  selector: 'wp-room',
  templateUrl: './room.component.html',
  styleUrls: ['room.component.css'],
})
export class RoomComponent implements OnInit, OnDestroy {
  // @ViewChild('localVideo', { static: true }) localVideo: ElementRef;
  roomId: string;
  userName: string;
  private roomConnectionstatusSubscribtion: Subscription;
  private readyToCallSubscription: Subscription;
  private localMediaInitialized: Observable<boolean>;
  private localMediaInitializedSubscription: Subscription;
  private getUserMediaFailedSubscription: Subscription;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public rtc: EasyRtcService,
    private location: Location,
    public snackBar: MatSnackBar,
    private settings: RtcSettingsService
  ) {
    const navigation = this.router.getCurrentNavigation();
    if (navigation.extras.state && navigation.extras.state.fromRoomJoin) {
      const urlSegments = this.router.url.split('/').filter((segment) => segment);
      if (urlSegments.length === 3 && urlSegments[0] === 'room') {
        const urlWithoutUsername = `/${urlSegments[0]}/${urlSegments[1]}`;
        this.location.go(urlWithoutUsername);
      }
    }
  }

  async ngOnInit() {
    this.roomId = this.route.snapshot.params['roomId'];
    this.userName = this.route.snapshot.params['userName'];
    if (!this.rtc.isConnectedToRoom.getValue()) {
      this.rtc.joinRoom(this.roomId, this.userName);
    }

    this.roomConnectionstatusSubscribtion = this.rtc.isConnectedToRoom.subscribe((connected) => {
      if (connected && this.settings.needsLocalMedia) {
        this.getUserMedia();
      }
    });

    this.readyToCallSubscription = this.rtc.isReadyToCall.subscribe(this.onReadyToCallChanged.bind(this));

    this.localMediaInitialized = this.rtc.isLocalMediaInitialized.pipe(
      filter((value) => value === true),
      take(1)
    );

    this.getUserMediaFailedSubscription = this.rtc.getUserMediaFailed.subscribe(this.onGetUserMediaFailed.bind(this));
  }

  public stopLocalMedia() {
    this.rtc.closeLocalMedia();
  }

  public getUserMedia(): Promise<void> {
    if (!this.rtc.isLocalMediaInitialized.getValue()) {
      return this.rtc
        .getLocalMedia()
        .then((stream) => {
          console.log('Acquired local media', stream);
        })
        .catch(() => {
          //Error handling is done via onUserMediaFailed
        });
    } else {
      return Promise.resolve();
    }
  }

  public async onGetUserMediaFailed(reason) {
    this.snackBar.open(`Access to user media failed: ${reason}\nTransmission of local media was disabled.`, 'dismiss', {
      duration: 8000,
      panelClass: ['getusermedia-failed-snackbar']
    });
    console.log('Access to user media failed: ', reason);
    this.settings.settings.sendAudio = false;
    this.settings.settings.sendVideo = false;
    await this.settings.applySettings();
    if(this.rtc.isReadyToCall.getValue() && this.rtc.needToCallUsers) {
      setTimeout(() => this.rtc.callOthersInRoom(), 500)
    }
  }

  public async onReadyToCallChanged(isReady: boolean) {
    if (isReady && this.rtc.needToCallUsers) {
      if (this.settings.needsLocalMedia) {
        this.localMediaInitializedSubscription = this.localMediaInitialized.subscribe((isInitialized) => {
          if (isInitialized) {
            this.rtc.callOthersInRoom();
          }
        }); //Make sure, the user media is ready befor calling the others
      } else {
        this.rtc.callOthersInRoom();
      }
    }
  }

  ngOnDestroy() {
    if (this.rtc.isConnectedToRoom.getValue()) {
      this.rtc.leave();
      this.roomConnectionstatusSubscribtion.unsubscribe();
      this.readyToCallSubscription.unsubscribe();
      this.localMediaInitializedSubscription?.unsubscribe();
      this.getUserMediaFailedSubscription.unsubscribe();
      this.stopLocalMedia();
    }
  }
}
