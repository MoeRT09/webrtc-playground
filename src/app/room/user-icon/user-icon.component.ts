import { Component, OnInit, Input, ViewEncapsulation, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { User } from '../../shared/model/user';
import { Subscription, BehaviorSubject } from 'rxjs';

export enum ConnectionState {
  error = 'error',
  connected = 'connected',
  connecting = 'connecting',
}

@Component({
  selector: 'wp-user-icon',
  templateUrl: './user-icon.component.html',
  styleUrls: ['./user-icon.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class UserIconComponent implements OnInit, OnDestroy {
  @Input() user: User;
  constructor(private cdRef: ChangeDetectorRef) {}
  private peerConnSubscription: Subscription;
  public connectionState: BehaviorSubject<ConnectionState> = new BehaviorSubject<ConnectionState>(ConnectionState.connecting);

  ngOnInit(): void {
    this.peerConnSubscription = this.user.onPeerConnectionEstablished$.subscribe((established) => {
      if (established) {
        this.user.peerconnection.addEventListener('iceconnectionstatechange', this.updateConnectionState.bind(this));
        this.user.peerconnection.addEventListener('signalingstatechange', this.updateConnectionState.bind(this));
      }
    });
  }

  ngOnDestroy(): void {
    this?.peerConnSubscription?.unsubscribe();
    this?.user?.peerconnection?.removeEventListener('iceconnectionstatechange', this.updateConnectionState.bind(this));
    this?.user?.peerconnection?.removeEventListener('signalingstatechange', this.updateConnectionState.bind(this));
  }

  private updateConnectionState(): void {
    if (this.user?.peerconnection?.signalingState === 'stable' && this.user?.peerconnection?.iceConnectionState === 'connected') {
      this.connectionState.next(ConnectionState.connected);
    } else if (
      this.user?.peerconnection?.signalingState === 'closed' ||
      this.user?.peerconnection?.iceConnectionState === 'failed' ||
      this.user?.peerconnection?.iceConnectionState === 'disconnected' ||
      this.user?.peerconnection?.iceConnectionState === 'completed'
    ) {
      this.connectionState.next(ConnectionState.error);
    } else {
      this.connectionState.next(ConnectionState.connecting);
    }

    this.cdRef.detectChanges();
    console.log("Connection state changed", this.connectionState.getValue());
  }
}
