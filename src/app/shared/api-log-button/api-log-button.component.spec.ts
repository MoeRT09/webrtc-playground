import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiLogButtonComponent } from './api-log-button.component';

describe('ApiLogButtonComponent', () => {
  let component: ApiLogButtonComponent;
  let fixture: ComponentFixture<ApiLogButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApiLogButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiLogButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
