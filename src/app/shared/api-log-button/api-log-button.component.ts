import { Component, OnInit, Input, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ApiInteractorService } from '../services/api-interactor.service';
import { ApiLogComponent } from '../../dialogs/api-log/api-log.component';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ApiInteractionLog } from '../model/api-interaction-log';

@Component({
  selector: 'wp-api-log-button',
  templateUrl: './api-log-button.component.html',
  styleUrls: ['./api-log-button.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApiLogButtonComponent implements OnInit, OnDestroy {
  constructor(public apiInteraction: ApiInteractorService, private dialog: MatDialog, private cdRef: ChangeDetectorRef) {}

  @Input() hideIfEmpty: boolean = false;
  private subscriptions = new Array<Subscription>();
  ngOnInit(): void {
    this.subscriptions.push(this.apiInteraction.apiLog$.subscribe(() => this.cdRef.detectChanges()));
    this.subscriptions.push(this.apiInteraction.interactionPending$.subscribe(() => this.cdRef.markForCheck()));
  }

  public showApiLog() {
    this.dialog.open(ApiLogComponent, {
      minWidth: '60%',
      maxWidth: '95vw',
    });
  }

  public apiLogErrors(): number {
    return this.apiInteraction.apiLog.filter((log) => log.type === 'error').length;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
