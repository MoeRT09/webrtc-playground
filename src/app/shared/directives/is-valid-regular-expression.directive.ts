import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

@Directive({
  selector: '[wpIsValidRegularExpression]',
  providers: [{provide: NG_VALIDATORS, useExisting: IsValidRegularExpressionDirective, multi: true, }]
})

export class IsValidRegularExpressionDirective implements Validator {

  constructor() { }


  public invalidRegExpValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      let valid = false;
      try {
        new RegExp(control.value);
        valid = true;
      }
      catch {}

      return valid ? null : {'validRegExp': {value: control.value}};
    };
  }

  validate(control: AbstractControl): ValidationErrors {
    return this.invalidRegExpValidator()(control);
  }

}
