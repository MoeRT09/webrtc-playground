// From: https://medium.com/front-end-weekly/angular-how-to-implement-conditional-custom-validation-1ec14b0feb45
import { Directive, Input, SimpleChanges } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidationErrors } from '@angular/forms';

@Directive({
  selector: '[requiredIf]',
  providers: [{ provide: NG_VALIDATORS, useExisting: RequiredIfDirective, multi: true }],
})
export class RequiredIfDirective implements Validator {
  @Input('requiredIf')
  requiredIf: boolean;

  validate(c: AbstractControl): ValidationErrors {
    let value = c.value;
    if ((value == null || value == undefined || value == '' || value?.length === 0) && this.requiredIf) {
      return {
        requiredIf: { condition: this.requiredIf },
      };
    }
    return null;
  }

  registerOnValidatorChange(fn: () => void): void { this._onChange = fn; }
 
  private _onChange: () => void;
 
  ngOnChanges(changes: SimpleChanges): void {
  
     if ('requiredIf' in changes) {
       
       if (this._onChange) this._onChange();
     }
   }
}
