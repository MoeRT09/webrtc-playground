import { Component, OnInit, Input, Injectable } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import { DataSource } from '@angular/cdk/collections/data-source';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { Clipboard } from '@angular/cdk/clipboard';
import { toUnicode } from 'punycode';

//Inspired by https://www.tutorialspoint.com/angular_material7/angular_material7_tree.htm
const OBJECT_PREVIEW_LENGTH = 16;
export class ObjectFlatNode {
  constructor(
    public expandable: boolean,
    public key: string,
    public value: any,
    public level: number,
    public type: string,
    public preview: string
  ) {}
}

export class ObjectNode {
  children?: ObjectNode[];
  key: string;
  value: any;
  type: string;
  preview?: string;
}

// const TEST_DATA = {
//   userName: 'polqa',
//   easyrtcid: 'LWcALzW8dqTJqld3',
//   isMe: false,
//   streams: [{ streamName: 'default' }],
//   apiField: { mediaIds: { fieldName: 'mediaIds', fieldValue: {} } },
// };

@Component({
  selector: 'wp-mat-dynamic-tree',
  templateUrl: './mat-dynamic-tree.component.html',
  styleUrls: ['./mat-dynamic-tree.component.scss'],
})
export class MatDynamicTreeComponent implements OnInit {
  @Input() data: object = {};
  @Input() typesToPrintPrototypePropertiesFor: Array<string> = [];
  @Input() functionsToExpand: Array<string> = [];
  @Input() typesToSkip: RegExp = new RegExp('(?!)');
  @Input() propertiesToSkip: RegExp = new RegExp('(?!)');
  @Input() propertyFormatter: (propertyKey: string, propertyValue: string) => string;

  treeControl: FlatTreeControl<ObjectFlatNode>;
  treeFlattener: MatTreeFlattener<ObjectNode, ObjectFlatNode>;
  dataSource: MatTreeFlatDataSource<ObjectNode, ObjectFlatNode>;

  constructor(public clipboard: Clipboard) {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this._getLevel, this._isExpandable, this._getChildren);
    this.treeControl = new FlatTreeControl<ObjectFlatNode>(this._getLevel, this._isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  }

  ngOnInit() {
    this.dataSource.data = this.objectToObjectNodeTree(this.data);
  }

  public refresh(): void {
    // this.dataSource.data = null;
    this.dataSource.data = this.objectToObjectNodeTree(this.data);
  }
  transformer = (node: ObjectNode, level: number) => {
    return new ObjectFlatNode(!!node.children, node.key, node.value, level, node.type, node.preview);
  };

  private _getLevel = (node: ObjectFlatNode) => node.level;
  private _isExpandable = (node: ObjectFlatNode) => node.expandable;
  private _getChildren = (node: ObjectNode): Observable<ObjectNode[]> => of(node.children);
  hasChild = (_: number, _nodeData: ObjectFlatNode) => _nodeData.expandable;

  private objectToObjectNodeTree(object: object, level: number = 0, parent: ObjectNode = null): ObjectNode[] {
    let keys: Array<string>;
    const type = object.constructor.name;
    if (this.typesToPrintPrototypePropertiesFor.some((typeToShowPropsFor) => typeToShowPropsFor === type)) {
      keys = [...Object.keys(object), ...Object.keys(Object.getPrototypeOf(object))];
    } else {
      keys = Object.keys(object);
    }

    if (parent) {
      parent.preview = Array.isArray(object) ? '[' : '{';
    }

    const nodes = keys.reduce<ObjectNode[]>((accumulator, key) => {
      if (key.match(this.propertiesToSkip)) {
        return accumulator;
      }
      // if(key === 'currentLocalDescription') {
      //   debugger;
      // }
      let value = object[key];
      const node = new ObjectNode();

      if (value === null || value === undefined) {
        node.key = key;
        if (value === null) {
          node.value = node.type = 'null';
        }
        if (value === undefined) {
          node.value = node.type = 'undefined';
        }
        if (parent) {
          parent.preview += `${node.key}: ${node.value}, `;
        }
        return accumulator.concat(node);
      }

      if (
        value.constructor.name === 'Function' &&
        this.functionsToExpand.some((functionToExpand) => functionToExpand === key)
      ) {
        // unsure, whether this is a good idea.
        const retVal = object[key]();
        if (!retVal) {
          return accumulator;
        } else {
          value = retVal;
        }
      }
      const type: string = value.constructor.name;
      if (type.match(this.typesToSkip)) {
        return accumulator;
      }

      node.key = key;
      node.type = type;
      if (value != null) {
        if (typeof value === 'object') {
          if (parent) {
            parent.preview += Array.isArray(value) ? `${node.key}: […], ` : `${node.key}: {…}, `;
          }
          node.children = this.objectToObjectNodeTree(value, level + 1, node);
        } else {
          if (parent) {
            let previewValue = value;
            if (typeof previewValue === 'string') {
              previewValue = `"${MatDynamicTreeComponent.truncate(previewValue, OBJECT_PREVIEW_LENGTH)}"`;
            }
            parent.preview += `${node.key}: ${previewValue}, `;
          }

          if (this.propertyFormatter) {
            value = this.propertyFormatter(key, value);
          }
          node.value = value;
        }
      }
      return accumulator.concat(node);
    }, []);
    if (parent) {
      parent.preview = parent.preview.replace(/, $/, '');
      parent.preview += Array.isArray(object) ? ']' : '}';
    }
    return nodes;
  }

  public copyValue(value: string) {
    const plainText = value.replace(/<[^>]*>/g, '');
    this.clipboard.copy(plainText);
  }

  private static truncate(str, n){
    return (str.length > n) ? str.substr(0, n-1) + '…' : str;
  };
}
