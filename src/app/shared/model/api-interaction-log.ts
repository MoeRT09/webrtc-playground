export interface ApiInteractionLog {
  timestamp: Date;
  type: 'success' | 'error';
  message: any;
}
