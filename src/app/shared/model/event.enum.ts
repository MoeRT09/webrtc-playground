export const enum Event {
    CONNECT = 'connect',
    DISCONNECT = 'disconnect',
    JOIN = 'join',
    LEAVE = 'leave',
    MESSAGE = 'message'
}
