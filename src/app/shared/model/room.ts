import { User } from './user';
import { Util } from '../util';
import { RetryablePromise } from '../retryable-promise';

export class Room {
  participants: User[];
  id: string;

  constructor(id: string, me: User) {
    this.id = id;
    me.isMe = true;
    this.participants = [me];
    this.participants;
  }

  public updateUserCollectionFromPerRoomData(data: Easyrtc_PerRoomData) {
    const userIdsFromUpdate = Object.keys(data);
    const storedUserIdsWithoutMe = this.getUserIdsWithoutMe();

    const newUserIds = Util.getArrayAExceptB(userIdsFromUpdate, storedUserIdsWithoutMe);
    const removedUserIds = Util.getArrayAExceptB(storedUserIdsWithoutMe, userIdsFromUpdate);
    const stayedUserIds = Util.getArrayAIntersectB(storedUserIdsWithoutMe, userIdsFromUpdate);

    newUserIds.forEach((newUserId) => {
      const current = data[newUserId];
      console.log('New User joined: ', current);
      const newUser = new User(current.username, current.easyrtcid);
      newUser.apiField = current.apiField;
      this.addUserToCollection(newUser);
    });

    removedUserIds.forEach((removedUserId) => {
      console.log('User with Id ' + removedUserId + ' left');
      this.removeUserFromCollection(removedUserId);
    });

    // Updating the rest
    stayedUserIds.forEach((stayedUserId) => {
      this.updateUserInCollection(stayedUserId, data[stayedUserId]);
    });
  }

  public getUserIdsWithoutMe(): Array<string> {
    return this.participants.filter((user) => !user.isMe).map((user) => user.easyrtcid);
  }

  public getUsersWithoutMe(): Array<User> {
    return this.participants.filter((user) => !user.isMe);
  }

  public getMe(): User {
    return this.participants.find((user) => user.isMe);
  }

  public getUserFromEasyrtcid(easyrtcid: string): Promise<User> {
    return RetryablePromise.retry<User>(
      5,
      (resolve, reject) => {
        const user = this.participants.find((User) => User.easyrtcid === easyrtcid);
        if (user) {
          resolve(user);
        } else {
          console.warn('User not yet initialized, retrying...');
          reject('User not yet initialized.');
        }
      },
      500
    );
  }

  public addUserToCollection(user: User) {
    this.participants.push(user);
  }

  public removeUserFromCollection(easyrtcid: string): Promise<void>;
  public removeUserFromCollection(user: User): Promise<void>;
  public async removeUserFromCollection(userOrId: User | string): Promise<void> {
    const userToRemove = await this.getUserFromIdOrObject(userOrId);
    Util.removeElementFromArray(userToRemove, this.participants);
  }

  public async updateUserInCollection(userIdToUpdate: string, newUserData: Easyrtc_PerPeerRoomData): Promise<void>;
  public async updateUserInCollection(userToUpdate: User, newUserData: Easyrtc_PerPeerRoomData): Promise<void>;
  public async updateUserInCollection(userOrId: User | string, newUserData: Easyrtc_PerPeerRoomData): Promise<void> {
    const userToUpdate = await this.getUserFromIdOrObject(userOrId);
    userToUpdate.updateUser(newUserData);
  }

  public async getUserFromIdOrObject(easyrtcidOrObject: string | User) {
    let user: User;
    if (typeof easyrtcidOrObject === 'string') {
      user = await this.getUserFromEasyrtcid(easyrtcidOrObject);
    } else {
      user = easyrtcidOrObject;
    }
    return user;
  }
}
