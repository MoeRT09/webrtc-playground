import { CodecConfig } from '../rtc-media-settings/rtc-codecs';
import { ApiInteractionSettings } from '../../dialogs/settings/api-interaction-settings';

export interface RtcSettings {
  localIceCandidateFilter?: string;
  remoteIceCandidateFilter?: string;
  sendAudio?: boolean;
  sendVideo?: boolean;
  fileToStream?: FileList;
  streamFile?: boolean;
  autoplayOnFirstParticipant?: boolean;
  autostartStatRecording?: boolean;
  preferredSendAudioCodec?: CodecConfig;
  preferredSendVideoCodec?: CodecConfig;
  preferredReceiveAudioCodec?: CodecConfig;
  preferredReceiveVideoCodec?: CodecConfig;
  preferredSendAudioBandwidth?: number;
  preferredSendVideoBandwidth?: number;
  preferredReceiveAudioBandwidth?: number;
  preferredReceiveVideoBandwidth?: number;
  apiInteractions?: Array<ApiInteractionSettings>;
  includeSdpInStatRecording?: boolean;
}
