import { Util } from '../util';
import { BehaviorSubject } from 'rxjs';
import { RtcStats } from '../rtc-stats/rtc-stats';
export class User {
  userName: string;
  easyrtcid: string;
  streams: Array<MediaStream>;
  apiField?: any;
  isMe: boolean;
  private _peerconnection: RTCPeerConnection;
  private _rtcStats: RtcStats;
  private _onPeerConnectionEstablished$: BehaviorSubject<boolean>;

  /**
   * Getter onPeerConnectionEstablished$
   * @return {BehaviorSubject<boolean>}
   */
  public get onPeerConnectionEstablished$(): BehaviorSubject<boolean> {
    return this._onPeerConnectionEstablished$;
  }

  public get peerconnection() {
    return this._peerconnection;
  }

  public set peerconnection(pc: RTCPeerConnection) {
    this._peerconnection = pc;
    this._rtcStats = new RtcStats(pc);
    if (pc && this._onPeerConnectionEstablished$.getValue() === false) {
      this._onPeerConnectionEstablished$.next(true);
    } else if (!pc && this._onPeerConnectionEstablished$.getValue()) {
      this._onPeerConnectionEstablished$.next(false);
    }
  }

  public get rtcStats() {
    return this._rtcStats;
  }

  constructor(userName: string, easyrtcId: string, isMe: boolean = false) {
    this.userName = userName;
    this.easyrtcid = easyrtcId;
    this.isMe = isMe;
    this.streams = new Array<MediaStream>();
    this._onPeerConnectionEstablished$ = new BehaviorSubject<boolean>(false);
  }

  public isMuted(): boolean {
    let isMuted = true;
    this.streams.forEach((stream) => {
      if (stream.getAudioTracks().find((audioTrack) => audioTrack.enabled !== false)) {
        isMuted = false;
        return;
      }
    });
    // there are no audio tracks
    return isMuted;
  }

  public toggleMute(): void {
    this.streams.forEach((stream) => {
      stream.getAudioTracks().forEach((audioTrack) => (audioTrack.enabled = !audioTrack.enabled));
    });
  }

  public hasAudioTracks(): boolean {
    return User.hasTracks(this.streams, 'audio');
  }

  public hasVideoTracks(): boolean {
    return User.hasTracks(this.streams, 'video');
  }

  private static hasTracks(streams: MediaStream[], kind: 'audio' | 'video' = null) {
    for (let stream of streams) {
      let getTracks: () => MediaStreamTrack[];

      if (kind === 'audio') {
        getTracks = stream.getAudioTracks;
      } else if (kind === 'video') {
        getTracks = stream.getVideoTracks;
      } else {
        getTracks = stream.getTracks;
      }

      if (getTracks.call(stream).length > 0) {
        return true;
      }
    }
    return false;
  }

  public addStream(stream: MediaStream): void {
    this.streams.push(stream);
  }

  public removeStream(stream: MediaStream): void {
    Util.removeElementFromArray(stream, this.streams);
  }

  public updateUser(newUserData: Easyrtc_PerPeerRoomData): void {
    this.easyrtcid = newUserData.easyrtcid;
    this.userName = newUserData.username;
    this.apiField = newUserData.apiField;
  }
}
