import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatePairStatsComponent } from './candidate-pair-stats.component';

describe('CandidatePairStatsComponent', () => {
  let component: CandidatePairStatsComponent;
  let fixture: ComponentFixture<CandidatePairStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidatePairStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatePairStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
