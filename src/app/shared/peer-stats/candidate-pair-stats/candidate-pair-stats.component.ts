import { Component, OnInit, Input } from '@angular/core';
import { PropertyComponentDictionary, StatPropertyType } from '../property-component-dictionary';

@Component({
  selector: 'wp-candidate-pair-stats',
  templateUrl: './candidate-pair-stats.component.html',
  styleUrls: ['../peer-stats.css']
})



export class CandidatePairStatsComponent implements OnInit {
  @Input() stat: any;
  constructor() { }

  public readonly propertiesToDisplay: PropertyComponentDictionary[] = [
    {
      propertyName: 'state',
      propertyLabel: 'State',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-state'
    },
    {
      propertyName: 'nominated',
      propertyLabel: 'Nominated',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-nominated'
    },
    {
      propertyName: 'bytesSent',
      propertyLabel: 'Bytes sent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-bytessent',
      displayAs: StatPropertyType.filesize
    },
    {
      propertyName: 'bytesReceived',
      propertyLabel: 'Bytes received',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-bytesreceived',
      displayAs: StatPropertyType.filesize
    },
    {
      propertyName: 'totalRoundTripTime',
      propertyLabel: 'Total round trip time',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-totalroundtriptime',
      displayAs: StatPropertyType.timeInSeconds
    },
    {
      propertyName: 'currentRoundTripTime',
      propertyLabel: 'Current round trip time',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-currentroundtriptime',
      displayAs: StatPropertyType.timeInSecondsAsMilliseconds
    },
    {
      propertyName: 'availableOutgoingBitrate',
      propertyLabel: 'Available outgoing bitrate',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-availableoutgoingbitrate',
      displayAs: StatPropertyType.bitrate
    },
    {
      propertyName: 'availableIncomingBitrate',
      propertyLabel: 'Available incoming bitrate',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-availableincomingbitrate',
      displayAs: StatPropertyType.bitrate
    },
    {
      propertyName: 'requestsReceived',
      propertyLabel: 'Connectivity check requests received',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-requestsreceived'
    },
    {
      propertyName: 'requestsSent',
      propertyLabel: 'Connectivity check requests sent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-requestssent'
    },
    {
      propertyName: 'responsesReceived',
      propertyLabel: 'Connectivity check responses received',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-responsesreceived'
    },
    {
      propertyName: 'responsesSent',
      propertyLabel: 'Connectivity check responses sent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-responsessent'
    },
    {
      propertyName: 'consentRequestsSent',
      propertyLabel: 'Consent requests sent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-consentrequestssent'
    },
    {
      propertyName: 'TransportId',
      propertyLabel: 'Transport Id',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-transportid'
    },
    {
      propertyName: 'localCandidateId',
      propertyLabel: 'Local Candidate Id',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-localCandidateid'
    },
    {
      propertyName: 'remoteCandidateId',
      propertyLabel: 'remote Candidate Id',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatepairstats-remotecandidateid'
    }
  ]
  
  ngOnInit(): void {
    
  }

}
