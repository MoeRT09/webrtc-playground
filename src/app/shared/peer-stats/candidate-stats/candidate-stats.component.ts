import { Component, OnInit, Input } from '@angular/core';
import { PropertyComponentDictionary } from '../property-component-dictionary';

@Component({
  selector: 'wp-candidate-stats',
  templateUrl: './candidate-stats.component.html',
  styles: [
  ]
})
export class CandidateStatsComponent implements OnInit {
  @Input() stat: any;
  constructor() { }

  readonly propertiesToDisplay: PropertyComponentDictionary[] = [
    {
      propertyName: 'address',
      propertyLabel: 'IP address',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatestats-address'
    },
    {
      propertyName: 'ip',
      propertyLabel: 'IP address (non standard name)'
    },
    {
      propertyName: 'protocol',
      propertyLabel: 'Protocol',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatestats-protocol'
    },
    {
      propertyName: 'port',
      propertyLabel: 'Port',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatestats-port'
    },
    {
      propertyName: 'candidateType',
      propertyLabel: 'Candidate type',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatestats-candidatetype'
    },
    {
      propertyName: 'url',
      propertyLabel: 'URL',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatestats-url'
    },
    {
      propertyName: 'priority',
      propertyLabel: 'Priority',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatestats-priority'
    },
    {
      propertyName: 'networkType',
      propertyLabel: 'Network Type (deprecated)',
    },
    {
      propertyName: 'transportId',
      propertyLabel: 'Transport Id',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcicecandidatestats-transportid'
    }
  ];

  ngOnInit(): void {
  }

}
