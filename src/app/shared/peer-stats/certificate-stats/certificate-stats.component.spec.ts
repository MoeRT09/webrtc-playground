import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificateStatsComponent } from './certificate-stats.component';

describe('CertificateStatsComponent', () => {
  let component: CertificateStatsComponent;
  let fixture: ComponentFixture<CertificateStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificateStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificateStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
