import { Component, OnInit, Input } from '@angular/core';
import { PropertyComponentDictionary } from '../property-component-dictionary';

@Component({
  selector: 'wp-certificate-stats',
  templateUrl: './certificate-stats.component.html',
  styles: [
  ]
})
export class CertificateStatsComponent implements OnInit {
  @Input() stat;
  constructor() { }

  public readonly propertyDictionary: PropertyComponentDictionary[] = [
    {
      propertyName: 'fingerprint',
      propertyLabel: 'Fingerprint',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtccertificatestats-fingerprint'
    },
    {
      propertyName: 'fingerprintAlgorithm',
      propertyLabel: 'Fingerprint algorithm',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtccertificatestats-fingerprintalgorithm'
    },
    {
      propertyName: 'base64Certificate',
      propertyLabel: 'Base64 certificate',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtccertificatestats-base64certificate'
    }
  ];
  ngOnInit(): void {
  }

}
