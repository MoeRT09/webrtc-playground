import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodecStatsComponent } from './codec-stats.component';

describe('CodecStatsComponent', () => {
  let component: CodecStatsComponent;
  let fixture: ComponentFixture<CodecStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodecStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodecStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
