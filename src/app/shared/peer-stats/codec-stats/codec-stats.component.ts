import { Component, OnInit, Input } from '@angular/core';
import { PropertyComponentDictionary } from '../property-component-dictionary';

@Component({
  selector: 'wp-codec-stats',
  templateUrl: './codec-stats.component.html',
  styles: [],
})
export class CodecStatsComponent implements OnInit {
  @Input() stat;
  constructor() {}

  public readonly propertyDictionary: PropertyComponentDictionary[] = [
    {
      propertyName: 'payloadType',
      propertyLabel: 'Payload type',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtccodecstats-payloadtype',
    },
    {
      propertyName: 'mimeType',
      propertyLabel: 'MIME-type',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtccodecstats-mimetype',
    },
    {
      propertyName: 'clockRate',
      propertyLabel: 'Clock rate',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtccodecstats-clockrate',
    },
    {
      propertyName: 'channels',
      propertyLabel: 'Audio channels',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtccodecstats-channels',
    },
    {
      propertyName: 'sdpFmtpLine',
      propertyLabel: 'SDP fmtp line',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtccodecstats-sdpfmtpline',
    },
  ];
  ngOnInit(): void {}
}
