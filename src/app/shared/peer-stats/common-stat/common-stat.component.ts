import { Component, OnInit, Input } from '@angular/core';
import { PropertyComponentDictionary, StatPropertyType } from '../property-component-dictionary';

@Component({
  selector: 'wp-common-stat',
  templateUrl: './common-stat.component.html',
  styleUrls: ['./common-stat.component.css']
})
export class CommonStatComponent implements OnInit {
  @Input() stat: RTCStats;

  readonly propertiesToDisplay: PropertyComponentDictionary[] = [
    {
      propertyName: 'id',
      propertyLabel: 'Id',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#idl-def-rtcstats'
    },
    {
      propertyName: 'timestamp',
      propertyLabel: 'Timestamp',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#idl-def-rtcstats',
      displayAs: StatPropertyType.timestamp
    },
    {
      propertyName: 'type',
      propertyLabel: 'Type',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#idl-def-rtcstats'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
