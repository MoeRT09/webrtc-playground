import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InboundRtpStatsComponent } from './inbound-rtp-stats.component';

describe('InboundRtpStatsComponent', () => {
  let component: InboundRtpStatsComponent;
  let fixture: ComponentFixture<InboundRtpStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InboundRtpStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboundRtpStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
