import { Component, OnInit, Input } from '@angular/core';
import { PropertyComponentDictionary, StatPropertyType } from '../property-component-dictionary';

@Component({
  selector: 'wp-inbound-rtp-stats',
  templateUrl: './inbound-rtp-stats.component.html',
  styles: [
  ]
})
export class InboundRtpStatsComponent implements OnInit {
  @Input() stat;
  constructor() { }

  public readonly propertyDictionary: PropertyComponentDictionary[] = [
    {
      propertyName: 'ssrc',
      propertyLabel: 'Synchronization source',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcrtpstreamstats-ssrc'
    },
    {
      propertyName: 'kind',
      propertyLabel: 'Kind',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcrtpstreamstats-kind'
    },
    {
      propertyName: 'packetsLost',
      propertyLabel: 'Packets lost',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcreceivedrtpstreamstats-packetslost'
    },
    {
      propertyName: 'packetsReceived',
      propertyLabel: 'Packets received',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcreceivedrtpstreamstats-packetsreceived'
    },
    {
      propertyName: 'bytesReceived',
      propertyLabel: 'Bytes received',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcreceivedrtpstreamstats-bytesreceived',
      displayAs: StatPropertyType.filesize
    },
    {
      propertyName: 'jitter',
      propertyLabel: 'Jitter',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcreceivedrtpstreamstats-jitter',
      displayAs: StatPropertyType.timeInSecondsAsMilliseconds
    },
    {
      propertyName: 'jitterBufferDelay',
      propertyLabel: 'Jitter buffer delay',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-jitterbufferdelay',
      displayAs: StatPropertyType.timeInSeconds
    },
    {
      propertyName: 'jitterBufferEmittedCount',
      propertyLabel: 'Number of frames/samples emitted from jitter buffer',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-jitterbufferemittedcount'
    },
    {
      propertyName: 'firCount',
      propertyLabel: 'Full Intra Request (FIR) packets received',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-fircount'
    },
    {
      propertyName: 'pliCount',
      propertyLabel: 'Picture Loss Indication (PLI) packets received',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-plicount'
    },
    {
      propertyName: 'qpSum',
      propertyLabel: 'Quantization Parameter (QP) values encoded',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-qpsum'
    },
    {
      propertyName: 'framesDecoded',
      propertyLabel: 'Frames decoded',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-framesdecoded'
    },
    {
      propertyName: 'keyFramesDecoded',
      propertyLabel: 'Key frames decoded',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-keyframesdecoded'
    },
    {
      propertyName: 'totalDecodeTime',
      propertyLabel: 'Total decode time',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-totaldecodetime',
      displayAs: StatPropertyType.timeInSeconds
    },
    {
      propertyName: 'nackCount',
      propertyLabel: 'Negative ACKnowledgement (NACK) packets received',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-nackcount'
    },
    {
      propertyName: 'headerBytesReceived',
      propertyLabel: 'Header bytes received',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-headerbytesreceived',
      displayAs: StatPropertyType.filesize
    },
    {
      propertyName: 'lastPacketReceivedTimestamp',
      propertyLabel: 'Timestamp of last received packet',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-lastpacketreceivedtimestamp',
      displayAs: StatPropertyType.highResTimestamp
    },
    {
      propertyName: 'estimatedPlayoutTimestamp',
      propertyLabel: 'Estimated playout time',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-estimatedplayouttimestamp',
      displayAs: StatPropertyType.highResTimestamp
    },
    {
      propertyName: 'fecPacketsReceived',
      propertyLabel: 'FEC packets received',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-fecpacketsreceived'
    },
    {
      propertyName: 'fecPacketsDiscarded',
      propertyLabel: 'FEC packets discarded',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-fecpacketsdiscarded'
    },
    {
      propertyName: 'totalInterFrameDelay',
      propertyLabel: 'Total inter frame delay',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-totalinterframedelay',
      displayAs: StatPropertyType.timeInSeconds
    },
    {
      propertyName: 'totalSquaredInterFrameDelay',
      propertyLabel: 'Total squared inter frame delay',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-totalsquaredinterframedelay',
      displayAs: StatPropertyType.timeInSeconds
    },
    {
      propertyName: 'totalAudioEnergy',
      propertyLabel: 'Total audio energy',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-jitterbufferemittedcount'
    },
    {
      propertyName: 'audioLevel',
      propertyLabel: 'Audio level',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-audiolevel'
    },
    {
      propertyName: 'totalSamplesDuration',
      propertyLabel: 'Total duration of received samples',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-totalsamplesduration',
      displayAs: StatPropertyType.timeInSeconds
    },
    {
      propertyName: 'totalSamplesReceived',
      propertyLabel: 'Total number of received samples',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-totalsamplesreceived'
    },
    {
      propertyName: 'concealedSamples',
      propertyLabel: 'Total number of concealed samples',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-concealedsamples'
    },
    {
      propertyName: 'silentConcealedSamples',
      propertyLabel: 'Total number of concealed samples that are silent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-silentconcealedsamples'
    },
    {
      propertyName: 'concealmentEvents',
      propertyLabel: 'Concealment events',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-concealmentevents'
    },
    {
      propertyName: 'insertedSamplesForDeceleration',
      propertyLabel: 'Inserted samples for deceleration',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-insertedsamplesfordeceleration'
    },
    {
      propertyName: 'removedSamplesForAcceleration',
      propertyLabel: 'Removed samples for accelleration',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-removedsamplesforacceleration'
    },
    {
      propertyName: 'decoderImplementation',
      propertyLabel: 'Decoder implementation',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-decoderimplementation'
    }
  ]
  ngOnInit(): void {
  }

}
