import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaSourceStatsComponent } from './media-source-stats.component';

describe('MediaSourceStatsComponent', () => {
  let component: MediaSourceStatsComponent;
  let fixture: ComponentFixture<MediaSourceStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaSourceStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaSourceStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
