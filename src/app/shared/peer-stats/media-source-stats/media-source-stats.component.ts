import { Component, OnInit, Input } from '@angular/core';
import { PropertyComponentDictionary, StatPropertyType } from '../property-component-dictionary';

@Component({
  selector: 'wp-media-source-stats',
  templateUrl: './media-source-stats.component.html',
  styles: [
  ]
})
export class MediaSourceStatsComponent implements OnInit {
  @Input() stat;
  constructor() { }

  public readonly propertyDictionary: PropertyComponentDictionary[] = [
    {
      propertyName: 'kind',
      propertyLabel: 'Kind',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcmediasourcestats-kind'
    },
    {
      propertyName: 'trackIdentifier',
      propertyLabel: 'Track identifier',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcmediasourcestats-trackidentifier'
    },
    {
      propertyName: 'audioLevel',
      propertyLabel: 'Audio level',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcaudiosourcestats-audiolevel'
    },
    {
      propertyName: 'totalAudioEnergy',
      propertyLabel: 'Total audio energy',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcaudiosourcestats-totalaudioenergy'
    },
    {
      propertyName: 'totalSamplesDuration',
      propertyLabel: 'Total duration of audio samples',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcaudiosourcestats-totalsamplesduration',
      displayAs: StatPropertyType.timeInSeconds
    },
    {
      propertyName: 'width',
      propertyLabel: 'Width of last frame',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcvideosourcestats-width'
    },
    {
      propertyName: 'height',
      propertyLabel: 'Height of last frame',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcvideosourcestats-height'
    },
    {
      propertyName: 'framesPerSecond',
      propertyLabel: 'Frames per second',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcvideosourcestats-framespersecond'
    }
  ]
  ngOnInit(): void {
  }

}
