import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutboundRtpStatsComponent } from './outbound-rtp-stats.component';

describe('OutboundRtpStatsComponent', () => {
  let component: OutboundRtpStatsComponent;
  let fixture: ComponentFixture<OutboundRtpStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutboundRtpStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutboundRtpStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
