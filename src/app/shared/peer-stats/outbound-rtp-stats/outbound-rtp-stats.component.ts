import { Component, OnInit, Input } from '@angular/core';
import { PropertyComponentDictionary, StatPropertyType } from '../property-component-dictionary';

@Component({
  selector: 'wp-outbound-rtp-stats',
  templateUrl: './outbound-rtp-stats.component.html',
  styles: [
  ]
})
export class OutboundRtpStatsComponent implements OnInit {
  @Input() stat;

  constructor() { }

  public readonly propertyDictionary: PropertyComponentDictionary[] = [
    {
      propertyName: 'ssrc',
      propertyLabel: 'Synchronization source',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcrtpstreamstats-ssrc'
    },
    {
      propertyName: 'kind',
      propertyLabel: 'Kind',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcrtpstreamstats-kind'
    },
    {
      propertyName: 'packetsSent',
      propertyLabel: 'Packets sent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcsentrtpstreamstats-packetssent'
    },
    {
      propertyName: 'bytesSent',
      propertyLabel: 'Bytes sent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcsentrtpstreamstats-bytessent',
      displayAs: StatPropertyType.filesize
    },
    {
      propertyName: 'headerBytesSent',
      propertyLabel: 'Header bytes sent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcsentrtpstreamstats-headerbytessent',
      displayAs: StatPropertyType.filesize
    },
    {
      propertyName: 'firCount',
      propertyLabel: 'Full Intra Request (FIR) packets received',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-fircount'
    },
    {
      propertyName: 'pliCount',
      propertyLabel: 'Picture Loss Indication (PLI) packets received',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-plicount'
    },
    {
      propertyName: 'qpSum',
      propertyLabel: 'Quantization Parameter (QP) values encoded',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-qpsum'
    },
    {
      propertyName: 'framesEncoded',
      propertyLabel: 'Frames encoded',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-framesencoded'
    },
    {
      propertyName: 'nackCount',
      propertyLabel: 'Negative ACKnowledgement (NACK) packets received',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-nackcount'
    },
    {
      propertyName: 'keyFramesEncoded',
      propertyLabel: 'Key frames encoded',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-keyframesencoded'
    },
    {
      propertyName: 'totalEncodeTime',
      propertyLabel: 'Total time spent encoding',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-totalencodetime',
      displayAs: StatPropertyType.timeInSeconds
    },
    {
      propertyName: 'totalEncodedBytesTarget',
      propertyLabel: 'Encoded bytes (Video)',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-totalencodedbytestarget',
      displayAs: StatPropertyType.filesize
    },
    {
      propertyName: 'frameWidth',
      propertyLabel: 'Width of last encoded frame',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-framewidth'
    },
    {
      propertyName: 'frameHeight',
      propertyLabel: 'Height of last encoded frame',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-frameheight'
    },
    {
      propertyName: 'framesPerSecond',
      propertyLabel: 'Frames per Second',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-framespersecond'
    },
    {
      propertyName: 'framesSent',
      propertyLabel: 'Frames sent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-framessent'
    },
    {
      propertyName: 'hugeFramesSent',
      propertyLabel: 'Huge frames sent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-hugeframessent'
    },
    {
      propertyName: 'totalPacketSendDelay',
      propertyLabel: 'Total packet send delay',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-totalpacketsenddelay',
      displayAs: StatPropertyType.timeInSeconds
    },
    {
      propertyName: 'qualityLimitationReason',
      propertyLabel: 'Quality limitation reason',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-qualitylimitationreason'
    },
    {
      propertyName: 'qualityLimitationResolutionChanges',
      propertyLabel: 'Resolution changes due to quality limitation',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-qualitylimitationresolutionchanges'
    },
    {
      propertyName: 'encoderImplementation',
      propertyLabel: 'Encoder implementation',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-encoderimplementation'
    },
    {
      propertyName: 'retransmittedBytesSent',
      propertyLabel: 'Retransmitted bytes sent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-retransmittedbytessent',
      displayAs: StatPropertyType.filesize
    },
    {
      propertyName: 'retransmittedPacketsSent',
      propertyLabel: 'Retransmitted packets sent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcoutboundrtpstreamstats-retransmittedpacketssent'
    },
    {
      propertyName: 'bitrateMean',
      propertyLabel: 'Mean bitrate',
      displayAs: StatPropertyType.bitrate
    },
    {
      propertyName: 'bitrateStdDev',
      propertyLabel: 'Bitrate standard deviation',
      displayAs: StatPropertyType.bitrate
    }
  ];
  ngOnInit(): void {
  }

}
