import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeerStatsComponent } from './peer-stats.component';

describe('PeerStatsComponent', () => {
  let component: PeerStatsComponent;
  let fixture: ComponentFixture<PeerStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeerStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeerStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
