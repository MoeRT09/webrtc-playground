import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import adapter from 'webrtc-adapter';
import { RtcStats } from '../rtc-stats/rtc-stats';
import { MatDialog } from '@angular/material/dialog';
import { RawStatsComponent } from '../../dialogs/raw-stats/raw-stats.component';

@Component({
  selector: 'wp-peer-stats',
  templateUrl: './peer-stats.component.html',
  styleUrls: ['./peer-stats-component.css'],
})
export class PeerStatsComponent implements OnInit, OnDestroy {
  @Input() rtcStats: RtcStats;

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {}

  public showRawStats(): void {
    this.dialog.open(RawStatsComponent, {
      data: this.rtcStats.connectionStats,
      minWidth: '60%'
    });
  }

  public trackById(index, stat) {
    return stat.id;
  }

  public mapToArrayValues<T>(map: Map<any, T>): Array<T> {
    return Array.from(map.values());
  }
}
