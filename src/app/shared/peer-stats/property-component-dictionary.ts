export interface PropertyComponentDictionary {
  propertyName: string,
  propertyLabel: string,
  propertyHelpLink?: string,
  displayAs?: StatPropertyType
}

export enum StatPropertyType {
  timeInSeconds = 'time-in-seconds',
  timeInSecondsAsMilliseconds = 'time-in-seconds-as-milliseconds',
  filesize = 'filesize',
  bitrate = 'bitrate',
  timestamp = 'timestamp',
  highResTimestamp = 'high-res-timestamp',
  formattedNumber = 'formatted-number'
}