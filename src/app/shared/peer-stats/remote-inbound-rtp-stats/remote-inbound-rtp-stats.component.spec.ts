import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoteInboundRtpStatsComponent } from './remote-inbound-rtp-stats.component';

describe('RemoteInboundRtpStatsComponent', () => {
  let component: RemoteInboundRtpStatsComponent;
  let fixture: ComponentFixture<RemoteInboundRtpStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoteInboundRtpStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteInboundRtpStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
