import { Component, OnInit, Input } from '@angular/core';
import { PropertyComponentDictionary, StatPropertyType } from '../property-component-dictionary';

@Component({
  selector: 'wp-remote-inbound-rtp-stats',
  templateUrl: './remote-inbound-rtp-stats.component.html',
  styles: [
  ]
})
export class RemoteInboundRtpStatsComponent implements OnInit {
  @Input() stat;
  constructor() { }

  ngOnInit(): void {
  }

  public readonly propertyDictionary: PropertyComponentDictionary[] = [
    {
      propertyName: 'ssrc',
      propertyLabel: 'Synchronization source',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcrtpstreamstats-ssrc'
    },
    {
      propertyName: 'kind',
      propertyLabel: 'Kind',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcrtpstreamstats-kind'
    },
    {
      propertyName: 'packetsLost',
      propertyLabel: 'Packets lost',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcreceivedrtpstreamstats-packetslost'
    },
    {
      propertyName: 'jitter',
      propertyLabel: 'Jitter',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcreceivedrtpstreamstats-jitter',
      displayAs: StatPropertyType.timeInSecondsAsMilliseconds
    },
    {
      propertyName: 'roundTripTime',
      propertyLabel: 'Round trip time',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcremoteinboundrtpstreamstats-roundtriptime',
      displayAs: StatPropertyType.timeInSecondsAsMilliseconds
    }
  ]

}
