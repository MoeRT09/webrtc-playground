import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoteOutboundRtpStatsComponent } from './remote-outbound-rtp-stats.component';

describe('RemoteOutboundRtpStatsComponent', () => {
  let component: RemoteOutboundRtpStatsComponent;
  let fixture: ComponentFixture<RemoteOutboundRtpStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoteOutboundRtpStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteOutboundRtpStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
