import { Component, OnInit, Input } from '@angular/core';
import { PropertyComponentDictionary, StatPropertyType } from '../property-component-dictionary';

@Component({
  selector: 'wp-remote-outbound-rtp-stats',
  templateUrl: './remote-outbound-rtp-stats.component.html',
  styles: [
  ]
})
export class RemoteOutboundRtpStatsComponent implements OnInit {
  @Input() stat;
  constructor() { }

  ngOnInit(): void {
  }

  public readonly propertyDictionary: PropertyComponentDictionary[] = [
    {
      propertyName: 'ssrc',
      propertyLabel: 'Synchronization source',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcrtpstreamstats-ssrc'
    },
    {
      propertyName: 'kind',
      propertyLabel: 'Kind',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcrtpstreamstats-kind'
    },
    {
      propertyName: 'packetsSent',
      propertyLabel: 'Packets sent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcsentrtpstreamstats-packetssent'
    },
    {
      propertyName: 'bytesSent',
      propertyLabel: 'Bytes sent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcsentrtpstreamstats-bytessent',
      displayAs: StatPropertyType.filesize
    }
  ]
}
