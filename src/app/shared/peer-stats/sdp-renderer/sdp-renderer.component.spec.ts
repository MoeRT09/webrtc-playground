import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SdpRendererComponent } from './sdp-renderer.component';

describe('SdpRendererComponent', () => {
  let component: SdpRendererComponent;
  let fixture: ComponentFixture<SdpRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SdpRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SdpRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
