import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'wp-sdp-renderer',
  templateUrl: './sdp-renderer.component.html',
  styleUrls: ['./sdp-renderer.component.css'],
  // encapsulation: ViewEncapsulation.ShadowDom
})
export class SdpRendererComponent implements OnInit {
  @Input() sessionDescription: string = "";

  public get formattedSessionDescription() {
    return SdpRendererComponent.sessionDescriptionFormatter(this.sessionDescription);
  }

  constructor() {}

  public static sessionDescriptionFormatter(sdp: string): string {
    let formatted = sdp;
    formatted = formatted.replace(/^(\w)=(.*?)((?:[: ]|$).*)/gm, '<span class="sdp-key warnColorFG"><strong>$1</strong></span>=<span class="sdp-value primaryColorFG"><strong>$2</strong>$3</span>');
    formatted = formatted.replace(/\n/g, '<br>');
    return formatted;
  }
  ngOnInit(): void {}
}
