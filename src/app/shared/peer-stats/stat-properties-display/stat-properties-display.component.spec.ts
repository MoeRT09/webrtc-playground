import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatProprtiesDisplayComponent } from './stat-properties-display.component';

describe('StatProprtiesDisplayComponent', () => {
  let component: StatProprtiesDisplayComponent;
  let fixture: ComponentFixture<StatProprtiesDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatProprtiesDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatProprtiesDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
