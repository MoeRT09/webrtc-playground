import { Component, OnInit, Input } from '@angular/core';
import { PropertyComponentDictionary } from '../property-component-dictionary';
import { Util } from '../../util';

@Component({
  selector: 'wp-stat-properties-display',
  templateUrl: './stat-properties-display.component.html',
  styleUrls: ['./stat-properties-display.component.css'],
})
export class StatProprtiesDisplayComponent implements OnInit {
  @Input() propertyDictionary: PropertyComponentDictionary[] = new Array<PropertyComponentDictionary>();
  @Input() stat: object;
  @Input() displayAll: boolean = false;
  @Input() excludeCommonFromUnlisted = true;
  public unlistedProperties: Array<string> = new Array<string>();

  private readonly commonProps = ['id', 'type', 'timestamp'];
  constructor() {}

  ngOnInit(): void {
    if (this.displayAll) {
      const listedKeys = this.propertyDictionary.map((property) => property.propertyName);
      const allKeys = Object.keys(this.stat);
      if (this.excludeCommonFromUnlisted) {
        this.unlistedProperties = Util.getArrayAExceptRest(allKeys, listedKeys, this.commonProps);
      } else {
        this.unlistedProperties = Util.getArrayAExceptB(allKeys, listedKeys);
      }
    }
  }
}
