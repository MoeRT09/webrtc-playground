import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatPropertyValueRendererComponent } from './stat-property-value-renderer.component';

describe('StatPropertyValueRendererComponent', () => {
  let component: StatPropertyValueRendererComponent;
  let fixture: ComponentFixture<StatPropertyValueRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatPropertyValueRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatPropertyValueRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
