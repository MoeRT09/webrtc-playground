import { Component, OnInit, Input } from '@angular/core';
import { StatPropertyType } from '../property-component-dictionary';

@Component({
  selector: 'wp-stat-property-value-renderer',
  templateUrl: './stat-property-value-renderer.component.html',
  styles: [
  ]
})
export class StatPropertyValueRendererComponent implements OnInit {
  @Input() displayAs: StatPropertyType;
  @Input() value: any;
  constructor() { }

  ngOnInit(): void {
  }

  public isNumber(value: any): boolean {
    return typeof value === 'number';
  }

  public isDate(value: any): boolean {
    return value instanceof Date;
  }

}
