import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatPropertyComponent } from './stat-property.component';

describe('StatPropertyComponent', () => {
  let component: StatPropertyComponent;
  let fixture: ComponentFixture<StatPropertyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatPropertyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
