import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'wp-stat-property',
  templateUrl: './stat-property.component.html',
  styleUrls: ['./stat-property.css']
})
export class StatPropertyComponent implements OnInit {
  @Input() label: string;
  @Input() helpLink: string = "";
  constructor() { }

  ngOnInit(): void {
  }

}
