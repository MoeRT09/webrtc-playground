import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryStatPropertyComponent } from './summary-stat-property.component';

describe('SummaryStatPropertyComponent', () => {
  let component: SummaryStatPropertyComponent;
  let fixture: ComponentFixture<SummaryStatPropertyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryStatPropertyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryStatPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
