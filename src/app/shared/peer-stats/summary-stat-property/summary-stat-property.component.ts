import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'wp-summary-stat-property',
  templateUrl: './summary-stat-property.component.html',
  styleUrls: ['./summary-stat-property.css']
})
export class SummaryStatPropertyComponent implements OnInit {
  @Input() label: string;
  @Input() helpLink: string = "";
  constructor() { }

  ngOnInit(): void {
  }

}
