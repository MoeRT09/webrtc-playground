import { Component, OnInit, Input } from '@angular/core';
import { SummaryStats } from '../../rtc-stats/summary-stats';
import { PropertyComponentDictionary, StatPropertyType } from '../property-component-dictionary';

@Component({
  selector: 'wp-summary-stats',
  templateUrl: './summary-stats.component.html',
  styleUrls: ['./summary-stats.component.css'],
})
export class SummaryStatsComponent implements OnInit {
  @Input() summaryStats: SummaryStats;
  constructor() {}

  public readonly connectionSummaryStats: PropertyComponentDictionary[] = [
    {
      propertyName: 'localIpAddress',
      propertyLabel: 'Local IP address',
    },
    {
      propertyName: 'localPort',
      propertyLabel: 'Local Port',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'localProtocol',
      propertyLabel: 'Local transport protocol',
    },
    {
      propertyName: 'localCandidateType',
      propertyLabel: 'Local candidate type',
    },
    {
      propertyName: 'remoteIpAddress',
      propertyLabel: 'Remote IP address',
    },
    {
      propertyName: 'remotePort',
      propertyLabel: 'Remote Port',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'remoteProtocol',
      propertyLabel: 'Remote transport protocol',
    },
    {
      propertyName: 'remoteCandidateType',
      propertyLabel: 'Remote candidate type',
    },
    {
      propertyName: 'outgoingBitrate',
      propertyLabel: 'Outgoing bitrate',
      displayAs: StatPropertyType.bitrate,
    },
    {
      propertyName: 'incomingBitrate',
      propertyLabel: 'Incoming bitrate',
      displayAs: StatPropertyType.bitrate,
    },
    {
      propertyName: 'availableOutgoingBitrate',
      propertyLabel: 'Available outgoing bitrate',
      displayAs: StatPropertyType.bitrate,
    },
    {
      propertyName: 'availableIncomingBitrate',
      propertyLabel: 'Available incoming bitrate',
      displayAs: StatPropertyType.bitrate,
    },
    {
      propertyName: 'rtpBytesSent',
      propertyLabel: 'RTP bytes sent',
      displayAs: StatPropertyType.filesize,
    },
    {
      propertyName: 'rtpBytesReceived',
      propertyLabel: 'RTP bytes received',
      displayAs: StatPropertyType.filesize,
    },
    {
      propertyName: 'currentRoundTripTime',
      propertyLabel: 'STUN Round Trip Time',
      displayAs: StatPropertyType.timeInSecondsAsMilliseconds,
    },
    {
      propertyName: 'dtlsCipher',
      propertyLabel: 'DTLS cipher',
    },
    {
      propertyName: 'srtpCipher',
      propertyLabel: 'SRTP cipher',
    },
    {
      propertyName: 'selectedCandidatePairChanges',
      propertyLabel: 'Selected candidate pair changes',
      displayAs: StatPropertyType.formattedNumber,
    },
  ];

  public readonly rtpSummaryStats: PropertyComponentDictionary[] = [
    {
      propertyName: 'ssrc',
      propertyLabel: 'SSRC',
    },
  ];

  public readonly outboundRtpSummaryStats: PropertyComponentDictionary[] = [
    {
      propertyName: 'packetsSent',
      propertyLabel: 'Packets sent',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'retransmittedPacketsSent',
      propertyLabel: 'Retransmitted packets sent',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'remotePacketsLost',
      propertyLabel: 'Packets lost at receiver',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'nackCount',
      propertyLabel: 'NACK count',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'bytesSent',
      propertyLabel: 'Bytes sent',
      displayAs: StatPropertyType.filesize,
    },
    {
      propertyName: 'headerBytesSent',
      propertyLabel: 'Header bytes sent',
      displayAs: StatPropertyType.filesize,
    },
    {
      propertyName: 'outgoingBitrate',
      propertyLabel: 'Outgoing bitrate',
      displayAs: StatPropertyType.bitrate,
    },
    {
      propertyName: 'rtpRoundTripTime',
      propertyLabel: 'RTP RTT',
      displayAs: StatPropertyType.timeInSecondsAsMilliseconds,
    },
    {
      propertyName: 'rtpJitter',
      propertyLabel: 'RTP Jitter',
      displayAs: StatPropertyType.timeInSecondsAsMilliseconds,
    },
    {
      propertyName: 'encoderImplementation',
      propertyLabel: 'Encoder implementation',
    },
  ];

  public readonly videoSummaryStats: PropertyComponentDictionary[] = [
    {
      propertyName: 'firCount',
      propertyLabel: 'FIR count',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'pliCount',
      propertyLabel: 'PLI count',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'qpSum',
      propertyLabel: 'QP sum',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'frameWidth',
      propertyLabel: 'Frame width',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'frameHeight',
      propertyLabel: 'Frame height',
      displayAs: StatPropertyType.formattedNumber,
    },
  ];

  public readonly outboundRtpVideoSummaryStats: PropertyComponentDictionary[] = [
    {
      propertyName: 'framesEncoded',
      propertyLabel: 'Frames encoded',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'framesPerSecond',
      propertyLabel: 'Frames per second',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'qualityLimitationReason',
      propertyLabel: 'Quality limitation reason',
    },
    {
      propertyName: 'qualityLimitationResolutionChanges',
      propertyLabel: 'Quality limitation resolution changes',
      displayAs: StatPropertyType.formattedNumber,
    },
  ];

  public readonly mediaSourceAudioSummaryStats: PropertyComponentDictionary[] = [
    {
      propertyName: 'audioLevel',
      propertyLabel: 'Source Audio level',
    },
    {
      propertyName: 'totalAudioEnergy',
      propertyLabel: 'Source total audio energy',
    },
    {
      propertyName: 'totalSamplesDuration',
      propertyLabel: 'Total duration of samples from source',
      displayAs: StatPropertyType.timeInSeconds,
    },
  ];

  public readonly mediaSourceVideoSummaryStats: PropertyComponentDictionary[] = [
    {
      propertyName: 'framesPerSecond',
      propertyLabel: 'Source frames per second',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'width',
      propertyLabel: 'Source frame width',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'height',
      propertyLabel: 'Source frame height',
      displayAs: StatPropertyType.formattedNumber,
    },
  ];

  public readonly codecSummaryStats: PropertyComponentDictionary[] = [
    {
      propertyName: 'payloadType',
      propertyLabel: 'Payload type',
    },
    {
      propertyName: 'mimeType',
      propertyLabel: 'MIME-type',
    },
    {
      propertyName: 'sdpFmtpLine',
      propertyLabel: 'SDP fmtp line',
    },
    {
      propertyName: 'clockRate',
      propertyLabel: 'Clock rate',
      displayAs: StatPropertyType.formattedNumber,
    },
  ];

  public readonly audioCodecSummaryStats: PropertyComponentDictionary[] = [
    {
      propertyName: 'channels',
      propertyLabel: 'Audio channels',
      displayAs: StatPropertyType.formattedNumber,
    },
  ];

  public readonly inboundRtpSummaryStats: PropertyComponentDictionary[] = [
    {
      propertyName: 'packetsReceived',
      propertyLabel: 'Packets received',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'packetsLost',
      propertyLabel: 'Packets lost',
      displayAs: StatPropertyType.formattedNumber,
    },
    {
      propertyName: 'bytesReceived',
      propertyLabel: 'Bytes received',
      displayAs: StatPropertyType.filesize,
    },
    {
      propertyName: 'headerBytesReceived',
      propertyLabel: 'Header bytes received',
      displayAs: StatPropertyType.filesize,
    },
    {
      propertyName: 'incomingBitrate',
      propertyLabel: 'Incoming bitrate',
      displayAs: StatPropertyType.bitrate,
    },
    {
      propertyName: 'jitter',
      propertyLabel: 'Jitter',
      displayAs: StatPropertyType.timeInSecondsAsMilliseconds,
    },
    {
      propertyName: 'decoderImplementation',
      propertyLabel: 'Decoder implementation',
    },
  ];

  public readonly inboundRtpAudioSummaryStats: PropertyComponentDictionary[] = [
    {
      propertyName: 'audioLevel',
      propertyLabel: 'Audio level',
    },
    {
      propertyName: 'totalAudioEnergy',
      propertyLabel: 'Total audio energy',
    },
  ];

  public readonly inboundRtpVideoSummaryStats: PropertyComponentDictionary[] = [
    {
      propertyName: 'framesDropped',
      propertyLabel: 'Frames dropped',
      displayAs: StatPropertyType.formattedNumber,
    },
  ];
  ngOnInit(): void {}
}
