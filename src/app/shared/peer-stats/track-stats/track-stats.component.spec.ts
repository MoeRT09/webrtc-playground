import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackStatsComponent } from './track-stats.component';

describe('TrackStatsComponent', () => {
  let component: TrackStatsComponent;
  let fixture: ComponentFixture<TrackStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
