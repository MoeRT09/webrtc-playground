import { Component, OnInit, Input } from '@angular/core';
import { PropertyComponentDictionary, StatPropertyType } from '../property-component-dictionary';

@Component({
  selector: 'wp-track-stats',
  templateUrl: './track-stats.component.html',
  styles: [
  ]
})
export class TrackStatsComponent implements OnInit {
  @Input() stat;
  constructor() { }

  ngOnInit(): void {
  }

  public readonly propertyDictionary: PropertyComponentDictionary[] = [
    {
      propertyName: 'kind',
      propertyLabel: 'Kind',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcmediahandlerstats-kind'
    },
    {
      propertyName: 'trackIdentifier',
      propertyLabel: 'Track identifier',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcmediahandlerstats-trackidentifier'
    },
    {
      propertyName: 'ended',
      propertyLabel: 'Ended',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcmediahandlerstats-ended'
    },// the Following properties will probably moved to inbound-rtp in the future as track stats ar made obsolete
    {
      propertyName: 'jitterBufferDelay',
      propertyLabel: 'Jitter buffer delay',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-jitterbufferdelay',
      displayAs: StatPropertyType.timeInSeconds
    },
    {
      propertyName: 'jitterBufferEmittedCount',
      propertyLabel: 'Number of frames/samples emitted from jitter buffer',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-jitterbufferemittedcount'
    },
    {
      propertyName: 'totalAudioEnergy',
      propertyLabel: 'Total audio energy',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-jitterbufferemittedcount'
    },
    {
      propertyName: 'audioLevel',
      propertyLabel: 'Audio level',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-audiolevel'
    },
    {
      propertyName: 'totalSamplesDuration',
      propertyLabel: 'Total duration of received samples',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-totalsamplesduration',
      displayAs: StatPropertyType.timeInSeconds
    },
    {
      propertyName: 'totalSamplesReceived',
      propertyLabel: 'Total number of received samples',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-totalsamplesreceived'
    },
    {
      propertyName: 'concealedSamples',
      propertyLabel: 'Total number of concealed samples',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-concealedsamples'
    },
    {
      propertyName: 'silentConcealedSamples',
      propertyLabel: 'Total number of concealed samples that are silent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-silentconcealedsamples'
    },
    {
      propertyName: 'concealmentEvents',
      propertyLabel: 'Concealment events',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-concealmentevents'
    },
    {
      propertyName: 'insertedSamplesForDeceleration',
      propertyLabel: 'Inserted samples for deceleration',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-insertedsamplesfordeceleration'
    },
    {
      propertyName: 'removedSamplesForAcceleration',
      propertyLabel: 'Removed samples for accelleration',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtcinboundrtpstreamstats-removedsamplesforacceleration'
    }
  ]

}
