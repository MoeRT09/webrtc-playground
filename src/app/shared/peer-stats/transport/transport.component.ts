import { Component, OnInit, Input } from '@angular/core';
import { PropertyComponentDictionary, StatPropertyType } from '../property-component-dictionary';

@Component({
  selector: 'wp-transport',
  templateUrl: './transport.component.html',
  styleUrls: ['../peer-stats.css'],
})
export class TransportComponent implements OnInit {
  @Input() stat: any;
  constructor() {}

  readonly propertiesToDisplay: PropertyComponentDictionary[] = [
    {
      propertyName: 'bytesSent',
      propertyLabel: 'Bytes sent',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtctransportstats-bytessent',
      displayAs: StatPropertyType.filesize
    },
    {
      propertyName: 'bytesReceived',
      propertyLabel: 'Bytes received',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtctransportstats-bytesreceived',
      displayAs: StatPropertyType.filesize
    },
    {
      propertyName: 'dtlsState',
      propertyLabel: 'DTLS State',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtctransportstats-dtlsstate'
    },
    {
      propertyName: 'dtlsCipher',
      propertyLabel: 'DTLS cipher',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtctransportstats-dtlscipher'
    },
    {
      propertyName: 'srtpCipher',
      propertyLabel: 'SRTP cipher',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtctransportstats-srtpcipher'
    },
    {
      propertyName: 'tlsVersion',
      propertyLabel: 'TLS version',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtctransportstats-tlsversion'
    },
    {
      propertyName: 'selectedCandidatePairChanges',
      propertyLabel: 'Selected Candidate Pair Changes',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtctransportstats-selectedcandidatepairchanges'
    },
    {
      propertyName: 'selectedCandidatePairId',
      propertyLabel: 'Selected Candidate Pair Id',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtctransportstats-selectedcandidatepairid'
    },
    {
      propertyName: 'localCertificateId',
      propertyLabel: 'Local certificate Id',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtctransportstats-localcertificateid'
    },
    {
      propertyName: 'remoteCertificateId',
      propertyLabel: 'Remote certificate Id',
      propertyHelpLink: 'https://w3c.github.io/webrtc-stats/#dom-rtctransportstats-remotecertificateid'
    }
  ]

  ngOnInit(): void {}
}
