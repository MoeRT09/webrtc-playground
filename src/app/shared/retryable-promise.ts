// https://stackoverflow.com/a/61257992
type promiseExecutor<T> = (resolve: (value?: T | PromiseLike<T>) => void, reject: (reason?: any) => void) => void;
export class RetryablePromise<T> extends Promise<T> {
  static retry<T>(retries: number, executor: promiseExecutor<T>, retryDelayMs: number = 0): Promise<T> {
    return new RetryablePromise(executor).catch(async (error) => {
      if (retries > 0) {
        if(retryDelayMs) {
          await RetryablePromise.wait(retryDelayMs);
        }
        return RetryablePromise.retry(retries - 1, executor, retryDelayMs);
      } else {
        return RetryablePromise.reject(error);
      }
    });
  }
  // https://stackoverflow.com/a/53452241
  public static wait(ms: number) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(ms);
      }, ms);
    });
  }
}
