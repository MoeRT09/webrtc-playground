import * as sdpTransform from 'sdp-transform';

export class RtcCodecs {
  public static getSupportedSendCodecs(kind: 'audio' | 'video'): RTCRtpCodecCapability[] {
    try {
      const codecs = RTCRtpSender?.getCapabilities(kind).codecs;
      return codecs;
    } catch (e) {
      return null;
    }
  }

  public static getSupportedReceiveCodecs(kind: 'audio' | 'video'): RTCRtpCodecCapability[] {
    try {
      const codecs = RTCRtpReceiver?.getCapabilities(kind).codecs;
      return codecs;
    } catch (e) {
      return null;
    }
  }

  private static getCodecConfigFromCapablilities(codecCapabilities: RTCRtpCodecCapability[]): Array<CodecConfig> {
    return codecCapabilities?.reduce<Array<CodecConfig>>((codecConfig, capability) => {
      const newConfig: CodecConfig = {
        name: RtcCodecs.getFriendlyCodecnameFromMimeType(capability.mimeType),
        codecCapability: capability,
      };

      codecConfig.push(newConfig);
      return codecConfig;
    }, []);
  }

  public static getSupportedSendCodecsAsCodecConfig(kind: 'audio' | 'video'): Array<CodecConfig> {
    return RtcCodecs.getCodecConfigFromCapablilities(RtcCodecs.getSupportedSendCodecs(kind));
  }

  public static getSupportedReceiveCodecsAsCodecConfig(kind: 'audio' | 'video'): Array<CodecConfig> {
    return RtcCodecs.getCodecConfigFromCapablilities(RtcCodecs.getSupportedReceiveCodecs(kind));
  }

  public static getFriendlyCodecnameFromMimeType(mimeType: string): string {
    const friendlyName = mimeType.substr(mimeType.lastIndexOf('/') + 1);

    return friendlyName;
  }

  public static getSdpFmtpLinesForCodec(mimeType: string, codecCapabilities: RTCRtpCodecCapability[]): Array<string> {
    return codecCapabilities.filter((codec) => codec.mimeType === mimeType).map((codec) => codec.sdpFmtpLine);
  }

  /** Sets the preferred codec for a media kind.
   *
   * @param {CodecConfig} codec The codec that should be used
   * @param {string} kind The type of media to use this codec for
   */

  public static preferCodec(codec: CodecConfig, kind: 'audio' | 'video'): (sdp: string) => string {
    return (sdp: string): string => {
      const parsedSdp = sdpTransform.parse(sdp);

      const mediaForKind = parsedSdp.media.filter((media) => media.type === kind);

      mediaForKind.forEach((media) => {
        const parsedPayloads = sdpTransform.parsePayloads(media.payloads);
        const payloadTypeOfPreferredCodec = RtcCodecs.getPayloadTypeForCodec(codec.codecCapability, media);

        const reorderedPayloads = RtcCodecs.putElementOnTopOfArray(payloadTypeOfPreferredCodec, parsedPayloads);

        if (codec.preferredSdpFmtpLine) {
          const fmtp = media.fmtp.find((fmtp) => fmtp.payload === payloadTypeOfPreferredCodec);

          if (fmtp) {
            console.log(`Changing fmtp line from ${fmtp.config} to ${codec.preferredSdpFmtpLine}`);
            fmtp.config = codec.preferredSdpFmtpLine;
          } else {
            media.fmtp.push({ payload: payloadTypeOfPreferredCodec, config: codec.preferredSdpFmtpLine });
          }
        }

        media.payloads = reorderedPayloads.join(' ');
      });

      const newSdp = sdpTransform.write(parsedSdp);
      console.log('Modified SDP according to codec filters: ', newSdp);
      return newSdp;
    };
  }
  /**Sets maximum bandwidth to be used for a media kind
   *
   * @param {number} preferredBandwidthKbps The maximum bandwidth in kbit/s. Set to null, to remove any potential existing bandwidth limits.
   * @param {string} kind The type of media to apply the limit
   */

  public static setBandwidth(preferredBandwidthKbps: number, kind: 'audio' | 'video'): (sdp: string) => string {
    return (sdp: string): string => {
      const parsedSdp = sdpTransform.parse(sdp);
      const mediaForKind = parsedSdp.media.filter((media) => media.type === kind);
      let wasModified = false;

      mediaForKind.forEach((media) => {
        const bandwidths = media.bandwidth;
        if (preferredBandwidthKbps !== undefined && preferredBandwidthKbps != null) {
          //from https://github.com/webrtc/samples/blob/gh-pages/src/content/peerconnection/bandwidth/js/main.js
          preferredBandwidthKbps = preferredBandwidthKbps >>> 0;
          if (bandwidths?.length > 0) {
            bandwidths.forEach((bandwidth) => {
              const bw = bandwidth.type === 'TIAS' ? preferredBandwidthKbps * 1000 : preferredBandwidthKbps;
              const unit = bandwidth.type === 'TIAS' ? 'bps' : 'kbps';
              console.log(`Updating bandwidth limit for ${kind} (${bandwidth.type}) from ${bandwidth.limit} ${unit} to ${bw} ${unit}`);
              bandwidth.limit = bw;
            });
          } else {
            console.log(`Setting bandwidth limit for ${kind} to ${preferredBandwidthKbps} kbps`);
            media.bandwidth = [
              {
                limit: preferredBandwidthKbps,
                type: 'AS',
              },
              {
                limit: preferredBandwidthKbps * 1000,
                type: 'TIAS',
              },
            ];
          }
          wasModified = true;
        } else {
          if (bandwidths?.length > 0) {
            console.log(`Removing bandwidth limit for ${kind}`);
            bandwidths.splice(0, bandwidths.length);
            wasModified = true;
          }
        }
      });

      if (wasModified) {
        const newSdp = sdpTransform.write(parsedSdp);
        console.log('SDP after applying bandwidth filter: ', newSdp);
        return newSdp;
      }

      return sdp;
    };
  }

  public static getPayloadTypeForCodec(codec: RTCRtpCodecCapability, mediaDescription: sdpTransform.MediaDescription): number {
    const plainCodecName = RtcCodecs.getFriendlyCodecnameFromMimeType(codec.mimeType).toLowerCase();
    const rtpMapOfFirstCodec = mediaDescription.rtp.find((rtpMap) => rtpMap.codec.toLowerCase() === plainCodecName);

    const possiblePayloadTypes = mediaDescription.rtp
      .filter((rtpMap) => rtpMap.codec.toLowerCase() === plainCodecName && rtpMap?.rate == codec.clockRate)
      .map((rtpMap) => rtpMap.payload);

    const defaultCodec = possiblePayloadTypes?.[0] !== undefined ? possiblePayloadTypes[0] : rtpMapOfFirstCodec.payload;

    if (!codec.sdpFmtpLine) {
      return defaultCodec;
    }

    const payloadTypeForFmtpLine = mediaDescription.fmtp
      .filter((fmtp) => possiblePayloadTypes.includes(fmtp.payload))
      .find((fmtp) => fmtp.config === codec.sdpFmtpLine);

    if (payloadTypeForFmtpLine === undefined) {
      console.warn('Payloadtype for the specified fmtp line could not be found. Falling back to Payload type ' + defaultCodec);
      return rtpMapOfFirstCodec.payload;
    }
    return payloadTypeForFmtpLine.payload;
  }

  public static putElementOnTopOfArray<T>(elementToPut: T, array: Array<T>): Array<T> {
    const newArray = new Array<T>();
    newArray.push(elementToPut);
    array.forEach((element) => {
      if (element !== elementToPut) {
        newArray.push(element);
      }
    });
    if (newArray.length !== array.length) {
      throw new Error('The Element to put on front of the array was no member of the array.');
    }
    return newArray;
  }

  public static codecCapabilitiesEquals(cap1: RTCRtpCodecCapability, cap2: RTCRtpCodecCapability): boolean {
    if (cap1 === cap2) {
      return true;
    }

    if (cap1 && cap2) {
      return (
        cap1.channels === cap2.channels &&
        cap1.clockRate === cap2.clockRate &&
        cap1.mimeType === cap2.mimeType &&
        cap1.sdpFmtpLine == cap2.sdpFmtpLine
      );
    }
    return false;
  }

  public static pipe = (...fns: Array<(string) => string>) => (initialVal: string) => fns.reduce((g, f) => f(g), initialVal);
}

export interface CodecConfig {
  name: string;
  codecCapability: RTCRtpCodecCapability;
  preferredSdpFmtpLine?: string;
}
