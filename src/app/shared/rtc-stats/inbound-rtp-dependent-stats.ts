export interface InboundRtpDependentStats {
  inboundRtpStats: any[],
  codecStats: Map<string, RTCStats>;
  trackStats: Map<string, any>;
  remoteOutboundRtpStats: Map<string, any>;
}
