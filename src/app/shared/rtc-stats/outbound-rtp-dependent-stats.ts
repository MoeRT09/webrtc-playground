export interface OutboundRtpDependentStats {
  outboundRtpStats: any[];
  codecStats: Map<string, RTCStats>;
  trackStats: Map<string, any>;
  remoteInboundRtpStats: Map<string, any>;
  mediaSourceStats: Map<string, any>;
}
