import { RtcStats } from '../rtc-stats';
import { SummaryStats } from '../summary-stats';
import { Subscription } from 'rxjs';

export class RecordStats {
  private _recordedStats: SummaryStats[];
  private _statSubscription: Subscription;
  private _isRecording: boolean = false;

  /**
   * Getter recordedStats
   * @return {SummaryStats[]}
   */
  public get recordedStats(): SummaryStats[] {
    return this._recordedStats;
  }

  /**
   * Getter isRecording
   * @return {boolean }
   */
  public get isRecording(): boolean {
    return this._isRecording;
  }

  constructor(private rtcStats: RtcStats) {
    this._recordedStats = new Array<SummaryStats>();
  }

  public start() {
    if (!this._statSubscription) {
      this._statSubscription = this.rtcStats.summaryStats.subscribe(this.onNewStat.bind(this));
      this._isRecording = true;
    }
  }

  private onNewStat(stat: SummaryStats): void {
    this._recordedStats.push(stat);
  }

  public stop() {
    if (this._statSubscription) {
      this._statSubscription.unsubscribe();
      this._statSubscription = null;
      this._isRecording = false;
    }
  }

  public clearRecordings() {
    this._recordedStats.splice(0, this._recordedStats.length);
  }
}
