//The default typescript typings for the RTCStatsType is wrong. This sets the correct types according to https://w3c.github.io/webrtc-stats/#dom-rtcstatstype
type RTCStatsTypeFixed =
  | 'codec'
  | 'inbound-rtp'
  | 'outbound-rtp'
  | 'remote-inbound-rtp'
  | 'remote-outbound-rtp'
  | 'media-source'
  | 'csrc'
  | 'peer-connection'
  | 'data-channel'
  | 'stream'
  | 'track'
  | 'transceiver'
  | 'sender'
  | 'receiver'
  | 'transport'
  | 'sctp-transport'
  | 'candidate-pair'
  | 'local-candidate'
  | 'remote-candidate'
  | 'certificate'
  | 'ice-server';

interface RTCIceCandidateStats extends RTCStats {
  transportId: string;
  address?: string;
  port?: number;
  protocol: string;
  candidateType: string;
  priority?: number;
  url?: number;
  relayProtocol?: string;
}
