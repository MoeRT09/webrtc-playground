/// <reference path="./rtc-stats-typings.d.ts" />
import adapter from 'webrtc-adapter';
import { Observable, timer } from 'rxjs';
import { switchMap, map, share } from 'rxjs/operators';
import { TransportDependentStats } from './transport-dependent-stats';
import { OutboundRtpDependentStats } from './outbound-rtp-dependent-stats';
import { InboundRtpDependentStats } from './inbound-rtp-dependent-stats';
import {
  SummaryStats,
  ConnectionSummaryStats,
  AudioCodecSummaryStats,
  VideoCodecSummaryStats,
  OutboundRtpAudioSummaryStats,
  RtpSummaryStats,
  OutboundRtpSummaryStats,
  OutboundRtpVideoSummaryStats,
  InboundRtpAudioSummaryStats,
  InboundRtpSummaryStats,
  InboundRtpVideoSummaryStats,
  VideoSummaryStats,
  MediaSourceAudioSummaryStats,
  MediaSourceVideoSummaryStats,
  MediaSourceSummaryStats,
} from './summary-stats';
import { RecordStats } from './record-stats/record-stats';

export class RtcStats {
  private _peerConnection: RTCPeerConnection;

  private _currentStatsReport: Map<string, RTCStats>;
  private _previousStatsReport: Map<string, RTCStats>;

  private _connectionStats$: Observable<Map<string, RTCStats>>;
  private _transportDependentStats: Observable<TransportDependentStats>;
  private _outboundRtpDependentStats: Observable<OutboundRtpDependentStats>;
  private _inboundRtpDependentStats: Observable<InboundRtpDependentStats>;
  private _summaryStats: Observable<SummaryStats>;

  private _statEmitInterval: number = 1000;
  private _statRecorder: RecordStats;

  /**
   * Getter statEmitInterval
   * @return {number }
   */
  public get statEmitInterval(): number {
    return this._statEmitInterval;
  }

  /**
   * Setter statEmitInterval
   * @param {number } value
   */
  public set statEmitInterval(value: number) {
    this._statEmitInterval = value;

    this._previousStatsReport = null;
    this.initializeObservables();
  }

  /**
   * Getter transportDependentStats
   * @return {Observable<TransportDependentStats>}
   */
  public get transportDependentStats(): Observable<TransportDependentStats> {
    return this._transportDependentStats;
  }

  /**
   * Getter outboundRtpDependentStats
   * @return {Observable<OutboundRtpDependentStats>}
   */
  public get outboundRtpDependentStats(): Observable<OutboundRtpDependentStats> {
    return this._outboundRtpDependentStats;
  }

  /**
   * Getter inboundRtpDependentStats
   * @return {Observable<InboundRtpDependentStats>}
   */
  public get inboundRtpDependentStats(): Observable<InboundRtpDependentStats> {
    return this._inboundRtpDependentStats;
  }

  /**
   * Getter summaryStats
   * @return {Observable<SummaryStats>}
   */
  public get summaryStats(): Observable<SummaryStats> {
    return this._summaryStats;
  }

  public get senders() {
    return this._peerConnection.getSenders();
  }
  public get receivers() {
    return this._peerConnection.getReceivers();
  }

  public get connectionStats(): Observable<Map<string, RTCStats>> {
    return this._connectionStats$;
  }

  /**
   * Getter statRecorder
   * @return {RecordStats}
   */
  public get statRecorder(): RecordStats {
    return this._statRecorder;
  }

  constructor(peerConnection: RTCPeerConnection) {
    this._peerConnection = peerConnection;

    this.initializeObservables();
    this._statRecorder = new RecordStats(this);
  }

  private initializeObservables() {
    this._connectionStats$ = this.getStatIntervalObservable(this._statEmitInterval);
    this._transportDependentStats = this._connectionStats$.pipe(map(this.getTransportDependendStats.bind(this)));
    this._outboundRtpDependentStats = this._connectionStats$.pipe(map(this.getOutboundRtpDependendStats.bind(this)));
    this._inboundRtpDependentStats = this._connectionStats$.pipe(map(this.getInboundRtpDependendStats.bind(this)));
    this._summaryStats = this._connectionStats$.pipe(
      map(this.getSummaryStats.bind(this) as (statsReport: Map<string, RTCStats>) => SummaryStats),
      share()
    );
  }

  private getStatIntervalObservable(intervalMs: number): Observable<Map<string, RTCStats>> {
    return timer(0, intervalMs).pipe(
      switchMap(() => {
        return this.getStats();
      }),
      share()
    );
  }

  private async getStats(): Promise<Map<string, RTCStats>> {
    this._previousStatsReport = this._currentStatsReport;
    this._currentStatsReport = (await this._peerConnection.getStats()) as Map<string, RTCStats>;
    console.log('getStats called');
    return this._currentStatsReport;
  }

  private getTransportDependendStats(statsReport: Map<string, RTCStats>): TransportDependentStats {
    const transportStats = RtcStats.getTransportStats(statsReport);
    const selectedCandidatePairStats = this.getSelectedCandidatePairStats(transportStats);
    const localCertificateStats = this.getLocalCertificateStats(transportStats);
    const remoteCertificateStats = this.getRemoteCertificateStats(transportStats);
    const localCandidateStats = this.getLocalCandidateStats(selectedCandidatePairStats);
    const remoteCandidateStats = this.getRemoteCandidateStats(selectedCandidatePairStats);

    return {
      transportStats: transportStats,
      selectedCandidatePairStats: selectedCandidatePairStats,
      localCertificateStats: localCertificateStats,
      remoteCertificateStats: remoteCertificateStats,
      localCandidateStats: localCandidateStats,
      remoteCandidateStats: remoteCandidateStats,
    };
  }

  private static getTransportStats(statsReport: Map<string, RTCStats>) {
    return RtcStats.getStatsByType(statsReport, 'transport');
  }

  private getSelectedCandidatePairStats(transportStats: RTCTransportStats[]) {
    const selectedCandidatePairStats = new Map<string, any>();
    if (transportStats.length > 0) {
      transportStats.forEach((transportStat) => {
        if (transportStat.selectedCandidatePairId) {
          const selectedCandidatePair: any = this._currentStatsReport.get(transportStat.selectedCandidatePairId);
          selectedCandidatePairStats.set(transportStat.id, selectedCandidatePair);
        }
      });
    } else {
      //Firefox does not currently support transport stats but has a 'selected' property on the candidate-pair
      const candidatePairs: any = RtcStats.getStatsByType(this._currentStatsReport, 'candidate-pair');
      const selectedCandidatePairs: RTCIceCandidatePairStats[] = candidatePairs.filter((candidatePair) => candidatePair?.selected === true);
      selectedCandidatePairs.forEach((candidatePair) => {
        selectedCandidatePairStats.set(candidatePair.id, candidatePair);
      });
    }
    return selectedCandidatePairStats;
  }

  private getLocalCertificateStats(transportStats: RTCTransportStats[]) {
    const localCertificateStats = new Map<string, RTCStats>();

    transportStats.forEach((transportStat) => {
      if (transportStat.localCertificateId) {
        const localCertificate: any = this._currentStatsReport.get(transportStat.localCertificateId);
        localCertificateStats.set(transportStat.id, localCertificate);
      }
    });
    return localCertificateStats;
  }

  private getRemoteCertificateStats(transportStats: RTCTransportStats[]) {
    const remoteCertificateStats = new Map<string, RTCStats>();

    transportStats.forEach((transportStat) => {
      if (transportStat.localCertificateId) {
        const remoteCertificate: any = this._currentStatsReport.get(transportStat.remoteCertificateId);
        remoteCertificateStats.set(transportStat.id, remoteCertificate);
      }
    });
    return remoteCertificateStats;
  }

  private getLocalCandidateStats(selectedCandidatePairStats: Map<string, RTCIceCandidatePairStats>) {
    const localCandidateStats = new Map<string, RTCIceCandidateStats>();
    selectedCandidatePairStats.forEach((candidatePair: RTCIceCandidatePairStats, transportId: string) => {
      if (candidatePair.localCandidateId) {
        const localCandidate: any = this._currentStatsReport.get(candidatePair.localCandidateId);
        localCandidateStats.set(transportId, localCandidate);
      }
    });
    return localCandidateStats;
  }

  private getRemoteCandidateStats(selectedCandidatePairStats: Map<string, RTCIceCandidatePairStats>) {
    const remoteCandidateStats = new Map<string, RTCIceCandidateStats>();
    selectedCandidatePairStats.forEach((candidatePair: RTCIceCandidatePairStats, transportId: string) => {
      if (candidatePair.remoteCandidateId) {
        const remoteCandidate: any = this._currentStatsReport.get(candidatePair.remoteCandidateId);
        remoteCandidateStats.set(transportId, remoteCandidate);
      }
    });
    return remoteCandidateStats;
  }

  private getOutboundRtpDependendStats(statsReport: Map<string, RTCStats>): OutboundRtpDependentStats {
    const outboundRtpStats = RtcStats.getOutboundRtpStats(statsReport);
    const codecStats = this.getCodecStats(outboundRtpStats);
    const trackStats = this.getTrackStats(outboundRtpStats);
    const remoteInboundRtpStats = this.getRemoteRtpStats(outboundRtpStats);
    const mediaSourceStats = this.getMediaSourceStats(outboundRtpStats);

    return {
      outboundRtpStats: outboundRtpStats,
      codecStats: codecStats,
      trackStats: trackStats,
      remoteInboundRtpStats: remoteInboundRtpStats,
      mediaSourceStats: mediaSourceStats,
    };
  }

  private getInboundRtpDependendStats(statsReport: Map<string, RTCStats>): InboundRtpDependentStats {
    const inboundRtpStats = RtcStats.getInboundRtpStats(statsReport);
    const codecStats = this.getCodecStats(inboundRtpStats);
    const trackStats = this.getTrackStats(inboundRtpStats);
    const remoteOutboundRtpStats = this.getRemoteRtpStats(inboundRtpStats);

    return {
      inboundRtpStats: inboundRtpStats,
      codecStats: codecStats,
      trackStats: trackStats,
      remoteOutboundRtpStats: remoteOutboundRtpStats,
    };
  }

  private static getOutboundRtpStats(statsReport: Map<string, RTCStats>): RTCOutboundRTPStreamStats[] {
    // If there goes something wrong during ICECandidate gathering, sometimes there are multiple rtp stats of the same kind, where one has no tracks associated.
    const stats = RtcStats.getStatsByType(statsReport, 'outbound-rtp');
    return stats;
  }

  private static getInboundRtpStats(statsReport: Map<string, RTCStats>): RTCInboundRTPStreamStats[] {
    // If there goes something wrong during ICECandidate gathering, sometimes there are multiple rtp stats of the same kind, where one has no tracks associated.
    const stats = RtcStats.getStatsByType(statsReport, 'inbound-rtp');
    return stats;
  }

  // This is highly ugly, but as long as https://github.com/w3c/webrtc-stats/issues/558 is not implemented by browsers,
  // there is no quick way to determine active senders/receivers. For now we assume, that the most recent stat entry in the report corresponds
  // to the active sender/receiver.
  private getCleanedRtpStats(stats: Array<RTCRTPStreamStats>): Array<RTCRTPStreamStats> {
    const inbound = stats.filter((stat) => (stat as any).type === 'inbound-rtp');
    const outbound = stats.filter((stat) => (stat as any).type === 'outbound-rtp');

    const cleanStats: Array<RTCRTPStreamStats> = new Array<RTCRTPStreamStats>();

    cleanStats.push(...this.getActiveOutboundRtpStats(outbound));

    cleanStats.push(...this.getActiveInboundRtpStats(inbound));

    return cleanStats;
  }

  private getActiveOutboundRtpStats(stats: Array<RTCOutboundRTPStreamStats>): Array<RTCOutboundRTPStreamStats> {
    const numberOfAudioStats = RtcStats.getStatCountByKind(stats, 'audio');
    const numberOfVideoStats = RtcStats.getStatCountByKind(stats, 'video');

    let cleanStats: RTCStats[] = stats;
    if (numberOfAudioStats > 1) {
      cleanStats = cleanStats.filter((stat) => (stat as any).kind === 'video' || (stat as any)?.trackId);
    }
    if (numberOfVideoStats > 1) {
      cleanStats = cleanStats.filter((stat) => (stat as any).kind === 'audio' || (stat as any)?.trackId);
    }

    return cleanStats;
  }

  private getActiveInboundRtpStats(stats: Array<RTCInboundRTPStreamStats>): Array<RTCInboundRTPStreamStats> {
    const activeReceiveSsrcs = new Set<string>();
    this._peerConnection.getReceivers().forEach((receiver) => {
      receiver.getSynchronizationSources().forEach((ssrc) => activeReceiveSsrcs.add(ssrc.source.toString()));
    });

    const filtered = stats.filter((stat) => activeReceiveSsrcs.has(stat?.ssrc.toString()));

    return filtered;
  }

  private static getStatCountByKind(stats: RTCOutboundRTPStreamStats[] | RTCInboundRTPStreamStats[], kind: 'audio' | 'video'): number {
    return RtcStats.filterStatsByKind(stats, kind).length;
  }

  private getCodecStats(stats: RTCOutboundRTPStreamStats[] | RTCInboundRTPStreamStats[]) {
    const codecStats = new Map<string, RTCStats>();
    stats.forEach((stat) => {
      if (stat.codecId) {
        const codec = this._currentStatsReport.get(stat.codecId);
        codecStats.set(stat.id, codec);
      }
    });

    return codecStats;
  }

  private getTrackStats(stats: RTCOutboundRTPStreamStats[] | RTCInboundRTPStreamStats[]) {
    const trackStats = new Map<string, RTCStats>();
    stats.forEach((stat) => {
      if (stat.trackId) {
        const track = this._currentStatsReport.get(stat.trackId);
        trackStats.set(stat.id, track);
      }
    });

    return trackStats;
  }

  private getRemoteRtpStats(stats: RTCOutboundRTPStreamStats[] | RTCInboundRTPStreamStats[]) {
    const remoteStats = new Map<string, RTCStats>();
    stats.forEach((stat) => {
      if (stat.remoteId) {
        const remote = this._currentStatsReport.get(stat.remoteId);
        remoteStats.set(stat.id, remote);
      }
    });

    return remoteStats;
  }

  private getMediaSourceStats(stats: RTCOutboundRTPStreamStats[]) {
    const mediaSourceStats = new Map<string, RTCStats>();
    stats.forEach((stat) => {
      if ((stat as any).mediaSourceId) {
        const mediaSourceStat = this._currentStatsReport.get((stat as any).mediaSourceId);
        mediaSourceStats.set(stat.id, mediaSourceStat);
      }
    });

    return mediaSourceStats;
  }

  private getSummaryStats(statsReport: Map<string, RTCStats>): SummaryStats {
    const summaryStats: SummaryStats = { timestamp: new Date() };

    summaryStats.connectionSummaryStats = this.getConnectionSummaryStats(statsReport);
    summaryStats.outboundRtpAudioSummaryStats = this.getOutboundRtpAudioSummaryStats(statsReport);
    summaryStats.outboundRtpVideoSummaryStats = this.getOutboundRtpVideoSummaryStats(statsReport);
    summaryStats.inboundRtpAudioSummaryStats = this.getInboundRtpAudioSummaryStats(statsReport);
    summaryStats.inboundRtpVideoSummaryStats = this.getInboundRtpVideoSummaryStats(statsReport);

    return summaryStats;
  }

  private getConnectionSummaryStats(statsReport: Map<string, RTCStats>): ConnectionSummaryStats {
    const summaryStats: ConnectionSummaryStats = {};

    const transportStats = RtcStats.getTransportStats(statsReport);
    const selectedCandidatePairStats = this.getSelectedCandidatePairStats(transportStats);
    const localCandidateStats = this.getLocalCandidateStats(selectedCandidatePairStats);
    const remoteCandidateStats = this.getRemoteCandidateStats(selectedCandidatePairStats);
    const inboundRtpStats = RtcStats.getInboundRtpStats(statsReport);
    const outboundRtpStats = RtcStats.getOutboundRtpStats(statsReport);
    const localDescription = this._peerConnection.currentLocalDescription;
    const remoteDescription = this._peerConnection.currentRemoteDescription;

    summaryStats.availableIncomingBitrate = RtcStats.getScalarFromStatsMap(selectedCandidatePairStats, 'availableIncomingBitrate');
    summaryStats.availableOutgoingBitrate = RtcStats.getScalarFromStatsMap(selectedCandidatePairStats, 'availableOutgoingBitrate');

    summaryStats.bytesSent = RtcStats.getScalarFromStatsMap(selectedCandidatePairStats, 'bytesSent');
    summaryStats.bytesReceived = RtcStats.getScalarFromStatsMap(selectedCandidatePairStats, 'bytesReceived');
    summaryStats.currentRoundTripTime = RtcStats.getScalarFromStatsMap(selectedCandidatePairStats, 'currentRoundTripTime');
    summaryStats.totalRoundTripTime = RtcStats.getScalarFromStatsMap(selectedCandidatePairStats, 'totalRoundTripTime');
    summaryStats.dtlsCipher = RtcStats.getScalarFromStatsArray(transportStats, 'dtlsCipher');
    summaryStats.srtpCipher = RtcStats.getScalarFromStatsArray(transportStats, 'srtpCipher');
    summaryStats.selectedCandidatePairChanges = RtcStats.getScalarFromStatsArray(transportStats, 'selectedCandidatePairChanges');

    summaryStats.localIpAddress = RtcStats.getIpAddressFromCandidateStat(localCandidateStats);
    summaryStats.remoteIpAddress = RtcStats.getIpAddressFromCandidateStat(remoteCandidateStats);
    summaryStats.localPort = RtcStats.getScalarFromStatsMap(localCandidateStats, 'port');
    summaryStats.remotePort = RtcStats.getScalarFromStatsMap(remoteCandidateStats, 'port');
    summaryStats.localProtocol = RtcStats.getScalarFromStatsMap(localCandidateStats, 'protocol');
    summaryStats.remoteProtocol = RtcStats.getScalarFromStatsMap(remoteCandidateStats, 'protocol');
    summaryStats.localCandidateType = RtcStats.getScalarFromStatsMap(localCandidateStats, 'candidateType');
    summaryStats.remoteCandidateType = RtcStats.getScalarFromStatsMap(remoteCandidateStats, 'candidateType');

    summaryStats.rtpBytesSent = RtcStats.getTotalRtpBytesSent(outboundRtpStats);
    summaryStats.rtpBytesReceived = RtcStats.getTotalRtpBytesReceived(inboundRtpStats);

    summaryStats.outgoingBitrate = this.getTotalRtpOutboundBitrate();
    summaryStats.incomingBitrate = this.getTotalRtpInboundBitrate();

    summaryStats.localSessionDescription = localDescription;
    summaryStats.remoteSessionDescription = remoteDescription;

    return RtcStats.returnNullIfEmptyObject(summaryStats);
  }

  private static isEmptyObject(obj: object): boolean {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
  }

  private static returnNullIfEmptyObject<T extends object>(obj: T): T | null {
    if (RtcStats.isEmptyObject(obj)) {
      return null;
    }

    return obj;
  }

  private static getTotalRtpBytesSent(stats: RTCOutboundRTPStreamStats[]): number {
    const sum = stats.reduce<number>((total, current) => {
      total += current?.bytesSent;

      const headerBytes = (current as any)?.headerBytesSent;
      if (headerBytes !== undefined) {
        total += headerBytes;
      }

      return total;
    }, 0);

    return sum;
  }

  private static getTotalRtpBytesReceived(stats: RTCInboundRTPStreamStats[]): number {
    const sum = stats.reduce<number>((total, current) => {
      total += current?.bytesReceived;

      const headerBytes = (current as any)?.headerBytesReceived;
      if (headerBytes !== undefined) {
        total += headerBytes;
      }

      return total;
    }, 0);

    return sum;
  }

  private getTotalRtpOutboundBitrate(): number {
    if (!this._previousStatsReport || !this._currentStatsReport) {
      return 0;
    }
    const currentOutboundRtp = RtcStats.getOutboundRtpStats(this._currentStatsReport);
    const previousOutboundRtp = RtcStats.getOutboundRtpStats(this._previousStatsReport);

    return RtcStats.getRtpOutboundBitrate(currentOutboundRtp, previousOutboundRtp);
  }

  private getTotalRtpInboundBitrate(): number {
    if (!this._previousStatsReport || !this._currentStatsReport) {
      return 0;
    }
    const currentInboundRtp = RtcStats.getInboundRtpStats(this._currentStatsReport);
    const previousInboundRtp = RtcStats.getInboundRtpStats(this._previousStatsReport);

    return RtcStats.getRtpInboundBitrate(currentInboundRtp, previousInboundRtp);
  }

  private getRtpOutboundBitrateByKind(kind: 'audio' | 'video'): number {
    if (this._previousStatsReport) {
      const outboundRtpStats = RtcStats.getOutboundRtpStats(this._currentStatsReport);
      const previousOutboundRtpStats = RtcStats.getOutboundRtpStats(this._previousStatsReport);
      const outboundRtpKindStats = RtcStats.filterStatsByKind(outboundRtpStats, kind);
      const previousOutboundRtpKindStats = RtcStats.filterStatsByKind(previousOutboundRtpStats, kind);
      return RtcStats.getRtpOutboundBitrate(outboundRtpKindStats, previousOutboundRtpKindStats);
    }

    return null;
  }
  private getRtpInboundBitrateByKind(kind: 'audio' | 'video'): number {
    if (this._previousStatsReport) {
      const inboundRtpStats = RtcStats.getInboundRtpStats(this._currentStatsReport);
      const previousInboundRtpStats = RtcStats.getInboundRtpStats(this._previousStatsReport);
      const inboundRtpKindStats = RtcStats.filterStatsByKind(inboundRtpStats, kind);
      const previousInboundRtpKindStats = RtcStats.filterStatsByKind(previousInboundRtpStats, kind);
      return RtcStats.getRtpInboundBitrate(inboundRtpKindStats, previousInboundRtpKindStats);
    }

    return null;
  }

  private static getRtpOutboundBitrate(
    currentOutboundRtpStats: RTCOutboundRTPStreamStats[],
    previousOutboundRtpStats: RTCOutboundRTPStreamStats[]
  ): number {
    const timeDiff = RtcStats.getTimestampDifferenceFromFirstStat(currentOutboundRtpStats, previousOutboundRtpStats);
    if (timeDiff === null) {
      return 0;
    }
    const currentBytes = RtcStats.getTotalRtpBytesSent(currentOutboundRtpStats);
    const previousBytes = RtcStats.getTotalRtpBytesSent(previousOutboundRtpStats);

    return RtcStats.calculateBitrate(currentBytes, previousBytes, timeDiff);
  }

  private static getRtpInboundBitrate(
    currentInboundRtpStats: RTCInboundRTPStreamStats[],
    previousInboundRtpStats: RTCInboundRTPStreamStats[]
  ): number {
    const timeDiff = RtcStats.getTimestampDifferenceFromFirstStat(currentInboundRtpStats, previousInboundRtpStats);
    if (timeDiff === null) {
      return 0;
    }
    const currentBytes = RtcStats.getTotalRtpBytesReceived(currentInboundRtpStats);
    const previousBytes = RtcStats.getTotalRtpBytesReceived(previousInboundRtpStats);

    return RtcStats.calculateBitrate(currentBytes, previousBytes, timeDiff);
  }

  private static getTimestampDifferenceFromFirstStat(currentStat: RTCStats[], previousStat: RTCStats[]): number {
    if (currentStat?.length > 0 && previousStat?.length > 0) {
      const currentTimestamp = currentStat[0].timestamp;
      const previousTimestamp = previousStat[0].timestamp;

      return currentTimestamp - previousTimestamp;
    }

    return null;
  }

  private static calculateBitrate(currentBytes: number, previousBytes: number, timeInMsBetweenMeasures): number {
    const bytesPerSecond = (currentBytes - previousBytes) * (1000 /*ms*/ / timeInMsBetweenMeasures);

    return bytesPerSecond * 8 /*bits/byte*/;
  }

  private static getIpAddressFromCandidateStat(candidateStat: Map<string, RTCIceCandidateStats>): string {
    let IpAddress = RtcStats.getScalarFromStatsMap(candidateStat, 'ip');
    if (IpAddress === undefined) {
      IpAddress = RtcStats.getScalarFromStatsMap(candidateStat, 'address');
    }

    return IpAddress;
  }

  private static getRtpCommonSummaryStats(stats: RTCOutboundRTPStreamStats[] | RTCInboundRTPStreamStats[]): RtpSummaryStats {
    const summaryStats: RtpSummaryStats = {};
    summaryStats.kind = RtcStats.getScalarFromStatsArray(stats, 'kind');
    summaryStats.ssrc = RtcStats.getScalarFromStatsArray(stats, 'ssrc');

    return summaryStats;
  }

  private static getOutboundRtpCommonSummaryStats(stats: RTCOutboundRTPStreamStats[], remoteStats: Map<string, RTCStats>): OutboundRtpSummaryStats {
    const commonRtpStats = RtcStats.getRtpCommonSummaryStats(stats);
    const summaryStats: OutboundRtpSummaryStats = { ...commonRtpStats };

    summaryStats.bytesSent = RtcStats.getScalarFromStatsArray(stats, 'bytesSent');
    summaryStats.encoderImplementation = RtcStats.getScalarFromStatsArray(stats, 'encoderImplementation');
    summaryStats.headerBytesSent = RtcStats.getScalarFromStatsArray(stats, 'headerBytesSent');
    summaryStats.nackCount = RtcStats.getScalarFromStatsArray(stats, 'nackCount');
    summaryStats.packetsSent = RtcStats.getScalarFromStatsArray(stats, 'packetsSent');
    summaryStats.retransmittedBytesSent = RtcStats.getScalarFromStatsArray(stats, 'retransmittedBytesSent');
    summaryStats.retransmittedPacketsSent = RtcStats.getScalarFromStatsArray(stats, 'retransmittedPacketsSent');
    summaryStats.rtpRoundTripTime = RtcStats.getScalarFromStatsMap(remoteStats, 'roundTripTime');
    summaryStats.rtpJitter = RtcStats.getScalarFromStatsMap(remoteStats, 'jitter');
    summaryStats.remotePacketsLost = RtcStats.getScalarFromStatsMap(remoteStats, 'packetsLost');

    return summaryStats;
  }

  private static getInboundRtpCommonSummaryStats(stats: RTCInboundRTPStreamStats[], trackStats: Map<string, RTCStats>): InboundRtpSummaryStats {
    const commonRtpStats = RtcStats.getRtpCommonSummaryStats(stats);

    const summaryStats: InboundRtpSummaryStats = { ...commonRtpStats };

    summaryStats.bytesReceived = RtcStats.getScalarFromStatsArray(stats, 'bytesReceived');
    summaryStats.packetsReceived = RtcStats.getScalarFromStatsArray(stats, 'packetsReceived');
    summaryStats.headerBytesReceived = RtcStats.getScalarFromStatsArray(stats, 'headerBytesReceived');
    summaryStats.packetsLost = RtcStats.getScalarFromStatsArray(stats, 'packetsLost');
    summaryStats.decoderImplementation = RtcStats.getScalarFromStatsArray(stats, 'decoderImplementation');
    summaryStats.jitter = RtcStats.getScalarFromStatsArray(stats, 'jitter');
    summaryStats.jitterBufferDelay = RtcStats.getScalarFromRtpOrTrackStats(stats, trackStats, 'jitterBufferDelay');

    return summaryStats;
  }

  private getOutboundRtpAudioSummaryStats(statsReport: Map<string, RTCStats>): OutboundRtpAudioSummaryStats {
    const outboundRtpStats = this.getCleanedRtpStats(RtcStats.getOutboundRtpStats(statsReport));

    const outboundRtpAudioStats = RtcStats.filterStatsByKind(outboundRtpStats, 'audio');
    if (outboundRtpAudioStats.length === 0) {
      return null;
    }
    const remoteInboundRtpAudioStats = this.getRemoteRtpStats(outboundRtpAudioStats);
    const outboundRtpCommonStats = RtcStats.getOutboundRtpCommonSummaryStats(outboundRtpAudioStats, remoteInboundRtpAudioStats);

    const outboundRtpAudioSummaryStats: OutboundRtpAudioSummaryStats = { ...outboundRtpCommonStats };

    outboundRtpAudioSummaryStats.codec = this.getCodecSummaryStats(outboundRtpAudioStats);
    outboundRtpAudioSummaryStats.mediaSourceStats = this.getMediaSourceSummaryStats(outboundRtpAudioStats);
    outboundRtpAudioSummaryStats.outgoingBitrate = this.getRtpOutboundBitrateByKind('audio');

    return RtcStats.returnNullIfEmptyObject(outboundRtpAudioSummaryStats);
  }

  private getInboundRtpAudioSummaryStats(statsReport: Map<string, RTCStats>): InboundRtpAudioSummaryStats {
    const inboundRtpStats = this.getCleanedRtpStats(RtcStats.getInboundRtpStats(statsReport));

    const inboundRtpAudioStats = RtcStats.filterStatsByKind(inboundRtpStats, 'audio');
    if (inboundRtpAudioStats.length === 0) {
      return null;
    }

    const trackStats = this.getTrackStats(inboundRtpAudioStats);
    const inboundRtpCommonStats = RtcStats.getInboundRtpCommonSummaryStats(inboundRtpAudioStats, trackStats);

    const inboundRtpAudioSummaryStats: InboundRtpAudioSummaryStats = { ...inboundRtpCommonStats };

    inboundRtpAudioSummaryStats.audioLevel = RtcStats.getScalarFromRtpOrTrackStats(inboundRtpAudioStats, trackStats, 'audioLevel');
    inboundRtpAudioSummaryStats.totalAudioEnergy = RtcStats.getScalarFromRtpOrTrackStats(inboundRtpAudioStats, trackStats, 'totalAudioEnergy');
    inboundRtpAudioSummaryStats.codec = this.getCodecSummaryStats(inboundRtpAudioStats);

    inboundRtpAudioSummaryStats.incomingBitrate = this.getRtpInboundBitrateByKind('audio');

    return RtcStats.returnNullIfEmptyObject(inboundRtpAudioSummaryStats);
  }

  private getOutboundRtpVideoSummaryStats(statsReport: Map<string, RTCStats>): OutboundRtpVideoSummaryStats {
    const outboundRtpStats = this.getCleanedRtpStats(RtcStats.getOutboundRtpStats(statsReport));

    const outboundRtpVideoStats = RtcStats.filterStatsByKind(outboundRtpStats, 'video');
    if (outboundRtpVideoStats.length === 0) {
      return null;
    }
    const remoteInboundRtpVideoStats = this.getRemoteRtpStats(outboundRtpVideoStats);
    const outboundRtpCommonStats = RtcStats.getOutboundRtpCommonSummaryStats(outboundRtpVideoStats, remoteInboundRtpVideoStats);
    const trackStats = this.getTrackStats(outboundRtpVideoStats);
    const videoSummaryStats = RtcStats.getVideoSummaryStats(outboundRtpVideoStats, trackStats);
    const outboundRtpVideoSummaryStats: OutboundRtpVideoSummaryStats = { ...outboundRtpCommonStats, ...videoSummaryStats };

    outboundRtpVideoSummaryStats.framesEncoded = RtcStats.getScalarFromStatsArray(outboundRtpVideoStats, 'framesEncoded');
    outboundRtpVideoSummaryStats.keyFramesEncoded = RtcStats.getScalarFromStatsArray(outboundRtpVideoStats, 'keyFramesEncoded');
    outboundRtpVideoSummaryStats.totalEncodeTime = RtcStats.getScalarFromStatsArray(outboundRtpVideoStats, 'totalEncodeTime');
    outboundRtpVideoSummaryStats.totalEncodedBytesTarget = RtcStats.getScalarFromStatsArray(outboundRtpVideoStats, 'totalEncodedBytesTarget');
    outboundRtpVideoSummaryStats.framesPerSecond = RtcStats.getScalarFromStatsArray(outboundRtpVideoStats, 'framesPerSecond');
    outboundRtpVideoSummaryStats.framesSent = RtcStats.getScalarFromStatsArray(outboundRtpVideoStats, 'framesSent');
    outboundRtpVideoSummaryStats.hugeFramesSent = RtcStats.getScalarFromStatsArray(outboundRtpVideoStats, 'hugeFramesSent');
    outboundRtpVideoSummaryStats.totalPacketSendDelay = RtcStats.getScalarFromStatsArray(outboundRtpVideoStats, 'totalPacketSendDelay');
    outboundRtpVideoSummaryStats.qualityLimitationReason = RtcStats.getScalarFromStatsArray(outboundRtpVideoStats, 'qualityLimitationReason');
    outboundRtpVideoSummaryStats.qualityLimitationResolutionChanges = RtcStats.getScalarFromStatsArray(
      outboundRtpVideoStats,
      'qualityLimitationResolutionChanges'
    );
    outboundRtpVideoSummaryStats.qualityLimitationResolutionChanges = RtcStats.getScalarFromStatsArray(
      outboundRtpVideoStats,
      'qualityLimitationResolutionChanges'
    );
    outboundRtpVideoSummaryStats.codec = this.getCodecSummaryStats(outboundRtpVideoStats);
    outboundRtpVideoSummaryStats.mediaSourceStats = this.getMediaSourceSummaryStats(outboundRtpVideoStats);

    outboundRtpVideoSummaryStats.outgoingBitrate = this.getRtpOutboundBitrateByKind('video');

    return RtcStats.returnNullIfEmptyObject(outboundRtpVideoSummaryStats);
  }
  private getInboundRtpVideoSummaryStats(statsReport: Map<string, RTCStats>): InboundRtpVideoSummaryStats {
    const inboundRtpStats = this.getCleanedRtpStats(RtcStats.getInboundRtpStats(statsReport));
    const inboundRtpVideoStats = RtcStats.filterStatsByKind(inboundRtpStats, 'video');
    if (inboundRtpVideoStats.length === 0) {
      return null;
    }
    const trackStats = this.getTrackStats(inboundRtpVideoStats);

    const inboundRtpCommonStats = RtcStats.getInboundRtpCommonSummaryStats(inboundRtpVideoStats, trackStats);
    const videoSummaryStats = RtcStats.getVideoSummaryStats(inboundRtpVideoStats, trackStats);

    const inboundRtpVideoSummaryStats: InboundRtpVideoSummaryStats = { ...inboundRtpCommonStats, ...videoSummaryStats };

    inboundRtpVideoSummaryStats.framesDropped = RtcStats.getScalarFromRtpOrTrackStats(inboundRtpVideoStats, trackStats, 'framesDropped');
    inboundRtpVideoSummaryStats.codec = this.getCodecSummaryStats(inboundRtpVideoStats);

    inboundRtpVideoSummaryStats.incomingBitrate = this.getRtpInboundBitrateByKind('video');

    return RtcStats.returnNullIfEmptyObject(inboundRtpVideoSummaryStats);
  }

  private static getVideoSummaryStats(
    stats: RTCOutboundRTPStreamStats[] | RTCInboundRTPStreamStats[],
    trackStats: Map<string, RTCStats>
  ): VideoSummaryStats {
    const summaryStats: VideoSummaryStats = {};

    summaryStats.frameWidth = RtcStats.getScalarFromRtpOrTrackStats(stats, trackStats, 'frameWidth');
    summaryStats.frameHeight = RtcStats.getScalarFromRtpOrTrackStats(stats, trackStats, 'frameHeight');
    summaryStats.firCount = RtcStats.getScalarFromStatsArray(stats, 'firCount');
    summaryStats.pliCount = RtcStats.getScalarFromStatsArray(stats, 'pliCount');
    summaryStats.qpSum = RtcStats.getScalarFromStatsArray(stats, 'qpSum');

    return summaryStats;
  }

  //This exists, because Chrome still uses Track stats, although the properties where moved to the rtp stats in the standard
  private static getScalarFromRtpOrTrackStats(
    rtpStats: RTCOutboundRTPStreamStats[] | RTCInboundRTPStreamStats[],
    trackStats: Map<string, RTCStats>,
    propertyName: string
  ) {
    const scalarFromRtpStats = RtcStats.getScalarFromStatsArray(rtpStats, propertyName);

    if (scalarFromRtpStats !== undefined) {
      return scalarFromRtpStats;
    } else {
      return RtcStats.getScalarFromStatsMap(trackStats, propertyName);
    }
  }

  private static filterStatsByKind<T extends RTCRTPStreamStats>(stats: Array<T>, kind: 'audio' | 'video'): Array<T> {
    return stats.filter((stat) => (stat as any).kind === kind);
  }

  private getCodecSummaryStats(stat: RTCOutboundRTPStreamStats[] | RTCInboundRTPStreamStats[]): AudioCodecSummaryStats | VideoCodecSummaryStats {
    const summaryStats: AudioCodecSummaryStats | VideoCodecSummaryStats = {};
    const codecStats: any = this.getCodecStats(stat);

    const channels = RtcStats.getScalarFromStatsMap(codecStats, 'channels');
    if (channels !== undefined) {
      (summaryStats as AudioCodecSummaryStats).channels = channels;
    }
    summaryStats.clockRate = RtcStats.getScalarFromStatsMap(codecStats, 'clockRate');
    summaryStats.mimeType = RtcStats.getScalarFromStatsMap(codecStats, 'mimeType');
    summaryStats.payloadType = RtcStats.getScalarFromStatsMap(codecStats, 'payloadType');
    summaryStats.sdpFmtpLine = RtcStats.getScalarFromStatsMap(codecStats, 'sdpFmtpLine');

    return summaryStats;
  }

  private getMediaSourceSummaryStats(stat: RTCOutboundRTPStreamStats[] | RTCInboundRTPStreamStats[]): MediaSourceSummaryStats {
    const mediaSourceStats: any = this.getMediaSourceStats(stat);

    const kind = RtcStats.getScalarFromStatsMap(mediaSourceStats, 'kind');
    if (kind === 'video') {
      const videoMediaSource: MediaSourceVideoSummaryStats = {
        framesPerSecond: RtcStats.getScalarFromStatsMap(mediaSourceStats, 'framesPerSecond'),
        height: RtcStats.getScalarFromStatsMap(mediaSourceStats, 'height'),
        width: RtcStats.getScalarFromStatsMap(mediaSourceStats, 'width'),
      };

      return videoMediaSource;
    } else if (kind === 'audio') {
      const audioMediaSource: MediaSourceAudioSummaryStats = {
        audioLevel: RtcStats.getScalarFromStatsMap(mediaSourceStats, 'audioLevel'),
        totalAudioEnergy: RtcStats.getScalarFromStatsMap(mediaSourceStats, 'totalAudioEnergy'),
        totalSamplesDuration: RtcStats.getScalarFromStatsMap(mediaSourceStats, 'totalSamplesDuration'),
      };

      return audioMediaSource;
    } else {
      return null;
    }
  }

  private static getPropertiesFromStatsMap(stats: Map<string, any>, propertyName: string) {
    const properties = new Map<string, any>();
    stats.forEach((stat, parentId) => {
      const property = stat[propertyName];
      properties.set(parentId, property);
    });

    return properties;
  }
  /**
   * Returns a scalar Value from a Map that contains entries of a specific property
   * @param propertyMap the value of the property mapped to the id of the parent stat
   * @returns the scalar value (if only one property is present in the map, returns the value itself, else a string of the values with the corresponding parent ids). Undefined, if the map is empty
   */
  private static propertyMapToScalar(propertyMap: Map<string, any>): any {
    let scalarProperty: any;
    for (let [parentId, property] of propertyMap.entries()) {
      if (property === undefined) {
        continue;
      }
      if (propertyMap.size === 1) {
        scalarProperty = property;
        break;
      }
      scalarProperty += `${property} (on parent stat with id "${parentId}"); `;
    }

    if (propertyMap.size > 1) {
      scalarProperty = String(scalarProperty).replace(/; $/, '');
    }

    return scalarProperty;
  }

  private static getScalarFromStatsArray(stats: Array<object>, propertyName: string) {
    let scalarProperty: any;

    for (let stat of stats) {
      const property = stat[propertyName];
      if (property === undefined) {
        continue;
      }

      if (stats.length === 1) {
        scalarProperty = property;
        break;
      }

      scalarProperty += `${property} (on stat with id "${stat['id']}"); `;
    }

    if (stats.length > 1) {
      scalarProperty = String(scalarProperty).replace(/; $/, '');
    }

    return scalarProperty;
  }

  private static getScalarFromStatsMap(stats: Map<string, any>, propertyName: string) {
    const properties = RtcStats.getPropertiesFromStatsMap(stats, propertyName);

    return RtcStats.propertyMapToScalar(properties);
  }

  private getSelectedCandidatePairFromSenderOrReceiver(
    kind: 'audio' | 'video',
    sendersOrReceivers: RTCRtpSender[] | RTCRtpReceiver[]
  ): RTCIceCandidatePair {
    if (sendersOrReceivers && sendersOrReceivers?.length > 0) {
      const senderOrReceiver = (sendersOrReceivers as Array<RTCRtpSender | RTCRtpReceiver>).find(
        (senderOrReceiver) => senderOrReceiver?.track?.kind === kind
      );
      if (!senderOrReceiver) {
        return null;
      }
      if (!senderOrReceiver?.transport?.iceTransport) {
        const error = `${adapter.browserDetails.browser} ${adapter.browserDetails.version} does not currently support the methods to get the selected candidate pairs.`;
        console.warn(error);
        throw new Error(error);
      }
      return senderOrReceiver?.transport?.iceTransport?.getSelectedCandidatePair();
    }
    return null;
  }

  public getSelectedCandidatePairForAudioSender(): RTCIceCandidatePair {
    return this.getSelectedCandidatePairFromSenderOrReceiver('audio', this._peerConnection.getSenders());
  }

  public getSelectedCandidatePairForVideoSender(): RTCIceCandidatePair {
    return this.getSelectedCandidatePairFromSenderOrReceiver('video', this._peerConnection.getSenders());
  }

  public getSelectedCandidatePairForAudioReceiver(): RTCIceCandidatePair {
    return this.getSelectedCandidatePairFromSenderOrReceiver('audio', this._peerConnection.getReceivers());
  }

  public getSelectedCandidatePairForVideoReceiver(): RTCIceCandidatePair {
    return this.getSelectedCandidatePairFromSenderOrReceiver('video', this._peerConnection.getReceivers());
  }
  /** This functions gets the IceCandidatePairs, that are currently in use.
   *
   * If streams are multiplexed, only the corresponding connection gets returned.
   * @returns {RTCIceCandidatePair[]} The candidatepairs in use
   */
  public getUniqueSelectedIceCandidatePairs(): RTCIceCandidatePair[] {
    const availablePairs = [
      this.getSelectedCandidatePairForAudioSender(),
      this.getSelectedCandidatePairForVideoSender(),
      this.getSelectedCandidatePairForAudioReceiver(),
      this.getSelectedCandidatePairForVideoReceiver(),
    ];

    const selectedPairs = availablePairs.reduce<RTCIceCandidatePair[]>((accumulator, current) => {
      if (
        current &&
        !accumulator.some((alreadyExisting) => current && alreadyExisting?.local === current?.local && alreadyExisting?.remote === current?.remote)
      ) {
        accumulator.push(current);
      }
      return accumulator;
    }, []);

    return selectedPairs;
  }

  public static getStatsByType(stats: RTCStatsReport, type: RTCStatsTypeFixed): Array<RTCStats> {
    const filteredStats = new Array<RTCStats>();
    stats.forEach((stat) => {
      if (stat.type === type) {
        filteredStats.push(stat);
      }
    });

    return filteredStats;
  }
}
