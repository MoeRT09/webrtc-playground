export interface SummaryStats {
  timestamp: Date;
  connectionSummaryStats?: ConnectionSummaryStats;
  outboundRtpAudioSummaryStats?: OutboundRtpAudioSummaryStats;
  outboundRtpVideoSummaryStats?: OutboundRtpVideoSummaryStats;
  inboundRtpAudioSummaryStats?: InboundRtpAudioSummaryStats;
  inboundRtpVideoSummaryStats?: InboundRtpVideoSummaryStats;
}

export interface ConnectionSummaryStats {
  localIpAddress?: string;
  remoteIpAddress?: string;
  localPort?: number;
  remotePort?: number;
  localProtocol?: string;
  remoteProtocol?: string;
  localCandidateType?: string;
  remoteCandidateType?: string;
  outgoingBitrate?: number;
  incomingBitrate?: number;
  availableOutgoingBitrate?: number;
  availableIncomingBitrate?: number;
  bytesSent?: number;
  bytesReceived?: number;
  currentRoundTripTime?: number;
  totalRoundTripTime?: number;
  dtlsCipher?: string;
  srtpCipher?: string;
  selectedCandidatePairChanges?: number;
  rtpBytesSent?: number;
  rtpBytesReceived?: number;
  localSessionDescription?: RTCSessionDescription;
  remoteSessionDescription?: RTCSessionDescription;
}

export interface RtpSummaryStats {
  ssrc?: string;
  kind?: string;
}

export interface VideoSummaryStats {
  firCount?: number;
  pliCount?: number;
  qpSum?: number;
  frameWidth?: number;
  frameHeight?: number;
}

export interface OutboundRtpSummaryStats extends RtpSummaryStats {
  packetsSent?: number;
  retransmittedPacketsSent?: number;
  bytesSent?: number;
  retransmittedBytesSent?: number;
  headerBytesSent?: number;
  outgoingBitrate?: number;
  nackCount?: number;
  encoderImplementation?: string;
  rtpRoundTripTime?: number;
  rtpJitter?: number;
  remotePacketsLost?: number;
}

export interface OutboundRtpVideoSummaryStats extends OutboundRtpSummaryStats, VideoSummaryStats {
  framesEncoded?: number;
  keyFramesEncoded?: number;
  totalEncodeTime?: number;
  totalEncodedBytesTarget?: number;
  framesPerSecond?: number;
  framesSent?: number;
  hugeFramesSent?: number;
  totalPacketSendDelay?: number;
  qualityLimitationReason?: string;
  qualityLimitationResolutionChanges?: number;
  mediaSourceStats?: MediaSourceVideoSummaryStats;
  codec?: VideoCodecSummaryStats;
}

export interface OutboundRtpAudioSummaryStats extends OutboundRtpSummaryStats {
  mediaSourceStats?: MediaSourceAudioSummaryStats;
  codec?: AudioCodecSummaryStats;
}

export interface MediaSourceSummaryStats {

}

export interface MediaSourceAudioSummaryStats extends MediaSourceSummaryStats {
  audioLevel?: number;
  totalAudioEnergy?: number;
  totalSamplesDuration?: number;
}

export interface MediaSourceVideoSummaryStats extends MediaSourceSummaryStats {
  framesPerSecond?: number;
  width?: number;
  height?: number;
}

export interface InboundRtpSummaryStats extends RtpSummaryStats {
  packetsReceived?: number;
  bytesReceived?: number;
  headerBytesReceived?: number;
  incomingBitrate?: number;
  packetsLost?: number;
  decoderImplementation?: string;
  jitter?: number;
  jitterBufferDelay?: number;
}

export interface InboundRtpVideoSummaryStats extends InboundRtpSummaryStats, VideoSummaryStats {
  framesDropped?: number;
  codec?: VideoCodecSummaryStats;
}

export interface InboundRtpAudioSummaryStats extends InboundRtpSummaryStats {
  audioLevel?: number;
  totalAudioEnergy?: number;
  codec?: AudioCodecSummaryStats;
}

export interface CodecSummaryStats {
  payloadType?: number;
  mimeType?: string;
  sdpFmtpLine?: string;
  clockRate?: number;
}

export interface AudioCodecSummaryStats extends CodecSummaryStats {
  channels?: number;
}

export interface VideoCodecSummaryStats extends CodecSummaryStats {}
