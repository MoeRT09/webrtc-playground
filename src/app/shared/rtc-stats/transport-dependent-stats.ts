export interface TransportDependentStats {
  transportStats: RTCTransportStats[];
  selectedCandidatePairStats: Map<string, RTCIceCandidatePairStats>;
  localCandidateStats: Map<string, RTCIceCandidateStats>;
  remoteCandidateStats: Map<string, RTCIceCandidateStats>;
  localCertificateStats: Map<string, RTCStats>;
  remoteCertificateStats: Map<string, RTCStats>;
}
