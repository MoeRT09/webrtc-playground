import { TestBed } from '@angular/core/testing';

import { ApiInteractorService } from './api-interactor.service';

describe('ApiInteractorService', () => {
  let service: ApiInteractorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiInteractorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
