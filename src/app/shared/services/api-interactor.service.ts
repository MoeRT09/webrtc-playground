import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject, defer } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ApiInteractionLog } from '../model/api-interaction-log';

@Injectable({
  providedIn: 'root',
})
export class ApiInteractorService {
  private _apiLog = new Array<ApiInteractionLog>();
  private _apiLogSubject = new Subject<ApiInteractionLog>();
  private _apiLog$: Observable<ApiInteractionLog>;
  private _interactionPendingSubject = new BehaviorSubject<boolean>(false);
  private _interactionPending$: Observable<boolean>;
  private _pendingRequests = 0;

  public get apiLog() {
    return this._apiLog;
  }

  public get apiLog$() {
    return this._apiLog$;
  }

  /**
   * Getter interactionPending$
   * @return {Observable<boolean>}
   */
  public get interactionPending$(): Observable<boolean> {
    return this._interactionPending$;
  }

  private set pendingRequests(value: number) {
    this._pendingRequests = value;
    this.updatePendingStatus();
  }

  private get pendingRequests() {
    return this._pendingRequests;
  }

  constructor(private http: HttpClient) {
    this._apiLog$ = this._apiLogSubject.asObservable();
    this._interactionPending$ = this._interactionPendingSubject.asObservable();
  }

  public sendRequest<T>(endpoint: ApiInteraction): Observable<T> {
    const result$ = this.http
      .request<T>(endpoint.method, endpoint.endpointUrl, endpoint.options)
      .pipe(this.doOnSubscribe(this.onRequestIssued.bind(this)), tap(this.onResult.bind(this), this.onError.bind(this)));

    return result$;
  }

  private onResult<T>(result: T) {
    const message: ApiInteractionLog = {
      type: 'success',
      message: result,
      timestamp: new Date(),
    };
    this.pendingRequests--;
    this._apiLog.unshift(message);
    this._apiLogSubject.next(message);
  }

  private onError(error: any) {
    const message: ApiInteractionLog = {
      type: 'error',
      message: error,
      timestamp: new Date(),
    };
    this.pendingRequests--;
    this._apiLog.unshift(message);
    this._apiLogSubject.next(message);
  }

  private onRequestIssued() {
    this.pendingRequests++;
  }

  private areRequestsPending(): boolean {
    return !!this._pendingRequests;
  }

  private updatePendingStatus() {
    const arePending = this.areRequestsPending();
    if (this._interactionPendingSubject.getValue() !== arePending) {
      this._interactionPendingSubject.next(arePending);
    }
  }

  private doOnSubscribe<T>(onSubscribe: () => void): (source: Observable<T>) => Observable<T> {
    return function inner(source: Observable<T>): Observable<T> {
      return defer(() => {
        onSubscribe();
        return source;
      });
    };
  }
}

export enum HttpMethod {
  delete = 'delete',
  get = 'get',
  head = 'head',
  options = 'options',
  post = 'post',
  put = 'put',
}

export interface ApiInteraction {
  method: HttpMethod;
  endpointUrl: string;
  options?: {
    body?: any;
    headers?: HttpHeaders | { [header: string]: string | string[] };
    observe?: 'body';
    params?: HttpParams | { [param: string]: string | string[] };
    responseType?: 'json';
    reportProgress?: boolean;
    withCredentials?: boolean;
  };
}
