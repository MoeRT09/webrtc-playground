import { TestBed } from '@angular/core/testing';

import { EasyRtcService } from './easy-rtc.service';

describe('EasyRtcService', () => {
  let service: EasyRtcService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EasyRtcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
