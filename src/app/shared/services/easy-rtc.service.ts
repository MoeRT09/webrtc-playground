/// <reference path="../../../types/easyrtc.d.ts" />
import { Injectable, OnDestroy } from '@angular/core';
import { environment } from '../../../environments/environment';
import * as socketIo from 'socket.io-client';
import Socket = SocketIOClient.Socket;
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { Event } from '../model/event.enum';
import { Room } from '../model/room';
import { User } from '../model/user';
import { RtcSettings } from '../model/rtc-settings';
import { RetryablePromise } from '../retryable-promise';
import { CodecConfig, RtcCodecs } from '../rtc-media-settings/rtc-codecs';
import { takeUntil } from 'rxjs/operators';
import { ApiInteractorService, ApiInteraction } from './api-interactor.service';
import { ApiInteractionSettings } from '../../dialogs/settings/api-interaction-settings';

@Injectable({
  providedIn: 'root',
})
export class EasyRtcService implements OnDestroy {
  public rtc: Easyrtc;
  private socket: Socket;
  private _socketServerUrl: string;
  private _room: Room;
  private _easyrtcId: string;
  private onDestroy$ = new Subject<boolean>();
  private _isConnectedToRoom: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private _isReadyToCall: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private _isLocalMediaInitialized: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private _getUserMediaFailed: Subject<string> = new Subject<string>();
  private _connectionWithAtLeastOneUserEstablished: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private _needToCallUsers: boolean = false;
  private _localCandidateFilter: RegExp = new RegExp('(?!)');
  private _remoteCandidateFilter: RegExp = new RegExp('(?!)');
  private _streamFromFile: boolean = false;
  private _fileToStream: File;
  private _localAudioEnabled: boolean = true;
  private _localVideoEnabled: boolean = true;
  private _autoplayStreamOnFirstParticipant: boolean = true;
  private _preferredSendAudioCodec: CodecConfig = undefined;
  private _preferredSendVideoCodec: CodecConfig = undefined;
  private _preferredReceiveAudioCodec: CodecConfig = undefined;
  private _preferredReceiveVideoCodec: CodecConfig = undefined;
  private _preferredSendAudioBandwidth: number = undefined;
  private _preferredSendVideoBandwidth: number = undefined;
  private _preferredReceiveAudioBandwidth: number = undefined;
  private _preferredReceiveVideoBandwidth: number = undefined;
  private _autoStartStatRecording: boolean = false;
  private _apiInteractions = new Array<ApiInteractionSettings>();
  private _shadowVideoElement: HTMLMediaElement;
  private _disconnectTriggerer = new Map<string, NodeJS.Timeout>();

  public get needToCallUsers() {
    return this._needToCallUsers;
  }
  public localStream: MediaStream;

  public get room(): Room {
    return this._room;
  }
  public get isConnectedToSocket(): boolean {
    return this.socket.connected;
  }

  public get isConnectedToRoom(): BehaviorSubject<boolean> {
    return this._isConnectedToRoom;
  }
  public get isLocalMediaInitialized(): BehaviorSubject<boolean> {
    return this._isLocalMediaInitialized;
  }

  public get isReadyToCall(): BehaviorSubject<boolean> {
    return this._isReadyToCall;
  }
  public get socketServerUrl() {
    return this._socketServerUrl;
  }

  /**
   * Getter connectionWithAtLeastOneUserEstablished
   * @return {BehaviorSubject<boolean> }
   */
  public get connectionWithAtLeastOneUserEstablished(): BehaviorSubject<boolean> {
    return this._connectionWithAtLeastOneUserEstablished;
  }

  public get getUserMediaFailed(): Subject<string> {
    return this._getUserMediaFailed;
  }


  /**
   * Getter localCandidateFilter
   * @return {RegExp }
   */
  public get localCandidateFilter(): RegExp {
    return this._localCandidateFilter;
  }

  /**
   * Getter remoteCandidateFilter
   * @return {RegExp }
   */
  public get remoteCandidateFilter(): RegExp {
    return this._remoteCandidateFilter;
  }

  /**
   * Getter streamFromFile
   * @return {boolean }
   */
  public get streamFromFile(): boolean {
    return this._streamFromFile;
  }

  public get canStream(): boolean {
    return this.streamFromFile && !!this._fileToStream && !!this._shadowVideoElement;
  }

  /**
   * Getter localAudioEnabled
   * @return {boolean }
   */
  public get localAudioEnabled(): boolean {
    return this._localAudioEnabled;
  }

  /**
   * Getter localVideoEnabled
   * @return {boolean }
   */
  public get localVideoEnabled(): boolean {
    return this._localVideoEnabled;
  }

  /**
   * Getter autoplayStreamOnFirstParticipant
   * @return {boolean }
   */
  public get autoplayStreamOnFirstParticipant(): boolean {
    return this._autoplayStreamOnFirstParticipant;
  }

  /**
   * Getter autoStartStatRecording
   * @return {boolean }
   */
  public get autoStartStatRecording(): boolean {
    return this._autoStartStatRecording;
  }

  constructor(private apiInteractor: ApiInteractorService) {
    if (!window.io) {
      window.io = socketIo;
    }
    this._socketServerUrl = environment.socketUrl;

    this.rtc = easyrtc;

    this.connectSocket();
    if (environment.production === false) {
      // this.rtc.enableDebug(true);
      console.log(this);
    }

    this.setupEventListeners();
  }

  public connectRtc(): Promise<void> {
    return new Promise<void>((resolve, reject) =>
      this.rtc.connect(
        'webrtc-playground',
        (easyrtcid, owner) => {
          console.log('Sucessfully connected. ID: ', easyrtcid, 'Owner: ', owner);
          this._easyrtcId = easyrtcid;
          resolve();
        },
        (errorCode, errorText) => {
          console.log('Failed connecting: ', errorText);
          reject();
        }
      )
    );
  }

  public connectSocket() {
    const uriMatch = this._socketServerUrl.match(new RegExp("((?:http|ws)(?:s?):\/\/.*?)((?:\/|$).*)"));
    const host = uriMatch?.[1] ? uriMatch[1] : this._socketServerUrl;
    const path = uriMatch?.[2] ? uriMatch[2] : null;

    this.socket = socketIo(host, { forceNew: true, path: path });

    this.rtc.useThisSocketConnection(this.socket);
  }

  private setupEventListeners() {
    this.rtc.setRoomOccupantListener(this.roomOccupantsListener.bind(this));
    this.rtc.setRoomEntryListener(this.roomEntryListener.bind(this));
    this.rtc.setStreamAcceptor(this.streamAcceptorListener.bind(this));
    this.rtc.setOnStreamClosed(this.streamClosedListener.bind(this));
    this.rtc.setOnError(this.errorListener.bind(this));
    this.rtc.setSignalingStateChangeListener(this.signallingStateChangeListener.bind(this));
    this.rtc.setIceConnectionStateChangeListener(this.iceConnectionStateChangeListener.bind(this));
    this.rtc.setDisconnectListener(this.disconnectListener.bind(this));
    this.rtc.setIceCandidateFilter(this.iceCandidateFilter.bind(this));
    this._connectionWithAtLeastOneUserEstablished
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(this.onConnectionWithAtLeastOneUserEstablished.bind(this));
  }

  private async signallingStateChangeListener(easyrtcid: string, eventTarget: globalThis.Event, signalingState: RTCSignalingState): Promise<void> {
    console.log('Signalling state changed: ', {
      easyrtcid,
      eventTarget,
      signalingState,
    });

    if (signalingState !== 'closed' && eventTarget && eventTarget instanceof RTCPeerConnection) {
      try {
        const user = await this.room.getUserFromEasyrtcid(easyrtcid);
        if (user.peerconnection !== eventTarget) {
          console.log('Got new Peerconnection for ', user.userName);
          user.peerconnection = eventTarget;
        }
      } catch {
        console.error('Failed to attach RTCCPeerConnection to user');
      }
    }
  }

  private iceConnectionStateChangeListener(easyrtcid: string, eventTarget: globalThis.Event, iceConnectionState: RTCIceConnectionState) {
    console.log('ICE Connection state changed: ', { easyrtcid, eventTarget, iceConnectionState });

    if (iceConnectionState === 'connected') {
      if (this._autoStartStatRecording) {
        this.startStatRecordingForAllUsers();
      }

      if (this._disconnectTriggerer.has(easyrtcid)) {
        clearTimeout(this._disconnectTriggerer.get(easyrtcid));
        this._disconnectTriggerer.delete(easyrtcid);
        console.log('The previously disconnected connection recovered.');
      }

      if (!this._connectionWithAtLeastOneUserEstablished.getValue()) {
        this._connectionWithAtLeastOneUserEstablished.next(true);
      }
    } else if (
      iceConnectionState !== 'checking' &&
      iceConnectionState !== 'new' &&
      this._connectionWithAtLeastOneUserEstablished.getValue() &&
      !this.checkIfConnectedToAtLeastOneUser()
    ) {
      if (iceConnectionState === 'disconnected') {
        if (!this._disconnectTriggerer.has(easyrtcid)) {
          console.warn('ICE Connection state is disconnected. Giving 30 seconds to recover.');
          const timeout = setTimeout(() => {
            if (this._connectionWithAtLeastOneUserEstablished.getValue() && !this.checkIfConnectedToAtLeastOneUser()) {
              this._connectionWithAtLeastOneUserEstablished.next(false);
            }
          }, 30000);
          this._disconnectTriggerer.set(easyrtcid, timeout);
        }
      } else {
        this._connectionWithAtLeastOneUserEstablished.next(false);
      }
    }
  }

  private checkIfConnectedToAtLeastOneUser(): boolean {
    const isConnected = !!this?.room?.getUsersWithoutMe()?.some((user) => user?.peerconnection?.iceConnectionState === 'connected');
    console.log('Is connected to at least one user: ', isConnected);
    return isConnected;
  }

  public startStatRecordingForAllUsers() {
    this?.room?.getUsersWithoutMe()?.forEach((user) => {
      if (!user?.rtcStats?.statRecorder?.isRecording) {
        user?.rtcStats?.statRecorder?.start();
      }
    });
  }
  /**This function can be used to filter out ICE-candidates.
   *
   * @param iceCandidate The ICE Candidate to handle. Has the form {type: "candidate", label: 0, id: "0", candidate: "candidate:2369930367 1 udp 2122194687 172.29.208.1 59322 typ host generation 0 ufrag lGVe network-id 2"}. See https://tools.ietf.org/id/draft-ietf-mmusic-ice-sip-sdp-14.html#rfc.section.5.1 for details.
   * @param receivedCandidate true if it is an iceCandidate is on recieved by a remote peer, false is it is a local candidate about to be sent out.
   *
   * @returns {RTCIceCandidate} returns an ICE candidate to be kept or null, if it should be dropped.
   */

  private iceCandidateFilter(iceCandidate: RTCIceCandidate, receivedCandidate: boolean): RTCIceCandidate {
    if (receivedCandidate) {
      if (iceCandidate?.candidate?.match(this._remoteCandidateFilter)) {
        console.info('Filtering out the following remote ICE candidate as it maches the filter: ', iceCandidate);
        iceCandidate = null;
      }
    } else {
      if (iceCandidate?.candidate?.match(this._localCandidateFilter)) {
        console.info('Filtering out the following local ICE candidate as it maches the filter: ', iceCandidate);
        iceCandidate = null;
      }
    }
    return iceCandidate;
  }

  private disconnectListener() {
    console.log('DisconnectListener called');
  }

  private async onConnectionWithAtLeastOneUserEstablished(isConnectedWithAtLeastOne: boolean) {
    const onFirstUserJoinInteractions = this?._apiInteractions?.filter((interaction) => interaction?.executeOnFirst);
    const onLastUserLeaveInteractions = this?._apiInteractions?.filter((interaction) => interaction?.executeOnLast);
    if (isConnectedWithAtLeastOne) {
      for (const interaction of onFirstUserJoinInteractions) {
        await this.interactWithApi(interaction);
      }
      if (this.canStream && this._autoplayStreamOnFirstParticipant && this.isFileStreamPaused()) {
        this.playFileStream();
      }
    } else {
      for (const interaction of onLastUserLeaveInteractions) {
        await this.interactWithApi(interaction);
      }
      if (this.canStream && this._autoplayStreamOnFirstParticipant && !this.isFileStreamPaused()) {
        this.pauseFileStream();
      }
    }
  }

  private async interactWithApi<T>(interaction: ApiInteraction) {
    console.log('Interacting with API:', interaction);
    try {
      const result = await this.apiInteractor.sendRequest<T>(interaction).toPromise();
      console.log('Got API result:', result);
      return result;
    } catch (e) {
      console.error('API interaction failed:', e);
    }
    return;
  }

  public async joinRoom(roomId: string, username: string): Promise<void> {
    console.log('setting username');
    this.rtc.setUsername(username);
    await this.connectRtc();

    const joinRoomPromise = new Promise<void>((resolve, reject) => {
      this.rtc.joinRoom(
        roomId,
        null,
        (roomName: string) => {
          this._room = new Room(roomId, new User(username, this._easyrtcId, true));
          this._isConnectedToRoom.next(true);
          console.log('Connected to room ', roomName);
          resolve();
        },
        (errorCode, errorText, roomName) => {
          console.log('Failed to join room: ', errorText);
          this._isConnectedToRoom.next(false);
          reject(errorText + '(' + errorCode + ')');
        }
      );
    });
    return joinRoomPromise;
  }

  private roomOccupantsListener(roomName: string, occupants: Easyrtc_PerRoomData, isOwner: boolean) {
    if (this._room && roomName == this._room.id) {
      console.log(occupants);
      this._room.updateUserCollectionFromPerRoomData(occupants);
      if (this._isReadyToCall.getValue() === false) {
        this._isReadyToCall.next(true);
      }
      if (this._connectionWithAtLeastOneUserEstablished.getValue() && !this.checkIfConnectedToAtLeastOneUser()) {
        this._connectionWithAtLeastOneUserEstablished.next(false);
      }
      console.log('Room occupants changed: ', this._room.participants);
    }
  }

  private async roomEntryListener(entering: boolean, roomName: string) {
    if (entering) {
      console.log('entering room', roomName);
      this._needToCallUsers = true;
    } else {
      console.log('leaving room', roomName);
      this._needToCallUsers = false;
    }
  }

  private async streamAcceptorListener(easyrtcid: string, stream: MediaStream, streamName: string) {
    const user = await this.room.getUserFromEasyrtcid(easyrtcid);
    if (!user) {
      console.warn('User not initialized before stream arrived!');
      return;
    }
    console.log('Accepting stream from ' + user.userName, stream);
    user.addStream(stream);
  }

  private async streamClosedListener(easyrtcid: string, stream: MediaStream, streamName: string) {
    try {
      const user = await this.room.getUserFromEasyrtcid(easyrtcid);
      console.log('Stream from ' + user.userName + ' is closed', stream);
      user.removeStream(stream);
    } catch {}
  }

  private errorListener(errorObject: { errorCode: string; errorText: string }): void {
    console.error(errorObject);
  }

  public leave(): Promise<void> {
    const leaveRoomPromise = new Promise<void>((resolve, reject) =>
      this.rtc.leaveRoom(
        this._room.id,
        (roomName: string) => {
          console.log('left room ', roomName);
          this.cleanup();
          resolve();
        },
        (errorCode, errorText, roomName) => {
          console.log('Failed to leave room: ', errorText);
          reject(errorText + '(' + errorCode + ')');
        }
      )
    );
    return leaveRoomPromise;
  }

  public getLocalMedia(mediaSourcName: string = 'default'): Promise<MediaStream> {
    return new Promise<MediaStream>((resolve, reject) => {
      this.rtc.initMediaSource(
        (localStream) => {
          this.localStream = localStream;
          this._room.getMe().addStream(localStream);
          this._isLocalMediaInitialized.next(true);
          resolve(localStream);
        },
        (errorCode, errorText) => {
          console.log('Failed getting local media: ', errorText);
          this._getUserMediaFailed.next(errorText);
          reject(errorText);
        },
        mediaSourcName
      );
    });
  }

  public closeLocalMedia(mediaSourceName: string = 'default') {
    if (this.localStream) {
      this.rtc.closeLocalStream(mediaSourceName);
      const me = this._room.getMe();
      me.streams.forEach((stream) => {
        const tracks = stream.getTracks();
        tracks.forEach((track) => {
          track.stop();
          stream.removeTrack(track);
        });
        me.removeStream(stream);
      });
      this.localStream = undefined;
      this._isLocalMediaInitialized.next(false);
    }
  }

  public async callOthersInRoom() {
    if (this._isReadyToCall.getValue()) {
      if (this._streamFromFile) {
        await this.initFileStream();
      }
      const otherParticipants = this.room.getUsersWithoutMe();
      otherParticipants.forEach((user) => {
        this.rtc.call(
          user.easyrtcid,
          (easyrtcid, mediaType) => {
            // call success
            console.log('Call to user ' + user.userName + 'with id ' + easyrtcid + ' successful');
            console.log('Media type: ', mediaType);
          },
          (errorCode, errorString) => {
            //call failed
            console.log('Call to user ' + user.userName + 'with id ' + user.easyrtcid + ' failed: ', errorCode, errorString);
          },
          (wasAccepted, easyrtcid) => {
            //call accepted
            if (wasAccepted) {
              console.log('User ' + user.userName + 'with id ' + easyrtcid + ' accepted call');
            } else {
              console.log('User ' + user.userName + 'with id ' + easyrtcid + ' rejected call');
            }
          },
          ['default']
        );
      });
      this._needToCallUsers = false;
    } else {
      console.log('Not ready for calling yet. Join a room first.');
    }
  }

  public async applySettings(settings: RtcSettings) {
    const streamUpdateNeeded = this.doNewSettingsRequireStreamUpdate(settings);
    const renegotiationNeeded = this.doNewSettingsRequireRenegotiation(settings);
    if (settings.localIceCandidateFilter !== undefined && settings.localIceCandidateFilter != null) {
      this._localCandidateFilter = new RegExp(settings.localIceCandidateFilter);
    }

    if (settings.remoteIceCandidateFilter !== undefined && settings.remoteIceCandidateFilter != null) {
      this._remoteCandidateFilter = new RegExp(settings.remoteIceCandidateFilter);
    }

    this._localAudioEnabled = !!settings?.sendAudio;
    this._localVideoEnabled = !!settings?.sendVideo;
    this.rtc.enableAudio(this._localAudioEnabled);
    this.rtc.enableVideo(this._localVideoEnabled);

    this._autoplayStreamOnFirstParticipant = !!settings?.autoplayOnFirstParticipant;
    this._autoStartStatRecording = !!settings.autostartStatRecording;

    if (settings?.apiInteractions) {
      this._apiInteractions = settings.apiInteractions;
    }

    this._streamFromFile = !!settings?.streamFile;
    if (settings?.streamFile && settings?.fileToStream && settings?.fileToStream?.length > 0) {
      this._fileToStream = settings?.fileToStream[0];
    }

    const outboundFilters = new Array<(sdp: string) => string>();
    const inboundFilters = new Array<(sdp: string) => string>();

    this._preferredSendAudioCodec = settings.preferredSendAudioCodec;
    if (settings.preferredSendAudioCodec) {
      outboundFilters.push(RtcCodecs.preferCodec(settings.preferredSendAudioCodec, 'audio'));
    }
    this._preferredSendVideoCodec = settings.preferredSendVideoCodec;
    if (settings.preferredSendVideoCodec) {
      outboundFilters.push(RtcCodecs.preferCodec(settings.preferredSendVideoCodec, 'video'));
    }
    this._preferredReceiveAudioCodec = settings.preferredReceiveAudioCodec;
    if (settings.preferredReceiveAudioCodec) {
      inboundFilters.push(RtcCodecs.preferCodec(settings.preferredReceiveAudioCodec, 'audio'));
    }
    this._preferredReceiveVideoCodec = settings.preferredReceiveVideoCodec;
    if (settings.preferredReceiveVideoCodec) {
      inboundFilters.push(RtcCodecs.preferCodec(settings.preferredReceiveVideoCodec, 'video'));
    }

    this._preferredSendAudioBandwidth = settings.preferredSendAudioBandwidth;
    if (settings.preferredSendAudioBandwidth !== undefined) {
      outboundFilters.push(RtcCodecs.setBandwidth(settings.preferredSendAudioBandwidth, 'audio'));
    }

    this._preferredSendVideoBandwidth = settings.preferredSendVideoBandwidth;
    if (settings.preferredSendVideoBandwidth !== undefined) {
      outboundFilters.push(RtcCodecs.setBandwidth(settings.preferredSendVideoBandwidth, 'video'));
    }

    this._preferredReceiveAudioBandwidth = settings.preferredReceiveAudioBandwidth;
    if (settings.preferredReceiveAudioBandwidth !== undefined) {
      inboundFilters.push(RtcCodecs.setBandwidth(settings.preferredReceiveAudioBandwidth, 'audio'));
    }

    this._preferredReceiveVideoBandwidth = settings.preferredReceiveVideoBandwidth;
    if (settings.preferredReceiveVideoBandwidth !== undefined) {
      inboundFilters.push(RtcCodecs.setBandwidth(settings.preferredReceiveVideoBandwidth, 'video'));
    }

    const outboundFilter = RtcCodecs.pipe(...outboundFilters);
    const inboundFilter = RtcCodecs.pipe(...inboundFilters);

    this.rtc.setSdpFilters(inboundFilter, outboundFilter);

    if (streamUpdateNeeded) {
      await this.updateStreamInCall();
    } else if (renegotiationNeeded) {
      this.renegotiate();
    }
  }

  public doNewSettingsRequireStreamUpdate(newSettings: RtcSettings): boolean {
    return (
      this.isConnectedToRoom.getValue() &&
      (!!this._streamFromFile !== !!newSettings?.streamFile ||
        this._fileToStream !== newSettings?.fileToStream?.[0] ||
        !!this._localAudioEnabled !== !!newSettings?.sendAudio ||
        !!this._localVideoEnabled !== !!newSettings?.sendVideo)
    );
  }

  public doNewSettingsRequireRenegotiation(newSettings: RtcSettings): boolean {
    return (
      this.isConnectedToRoom.getValue() &&
      (!RtcCodecs.codecCapabilitiesEquals(this?._preferredSendAudioCodec?.codecCapability, newSettings?.preferredSendAudioCodec?.codecCapability) ||
        !RtcCodecs.codecCapabilitiesEquals(this?._preferredSendVideoCodec?.codecCapability, newSettings?.preferredSendVideoCodec?.codecCapability) ||
        !RtcCodecs.codecCapabilitiesEquals(
          this?._preferredReceiveAudioCodec?.codecCapability,
          newSettings?.preferredReceiveAudioCodec?.codecCapability
        ) ||
        !RtcCodecs.codecCapabilitiesEquals(
          this?._preferredReceiveVideoCodec?.codecCapability,
          newSettings?.preferredReceiveVideoCodec?.codecCapability
        ) ||
        this?._preferredSendAudioCodec?.preferredSdpFmtpLine !== newSettings?.preferredSendAudioCodec?.preferredSdpFmtpLine ||
        this?._preferredSendVideoCodec?.preferredSdpFmtpLine !== newSettings?.preferredSendVideoCodec?.preferredSdpFmtpLine ||
        this?._preferredReceiveAudioCodec?.preferredSdpFmtpLine !== newSettings?.preferredReceiveAudioCodec?.preferredSdpFmtpLine ||
        this?._preferredReceiveVideoCodec?.preferredSdpFmtpLine !== newSettings?.preferredReceiveVideoCodec?.preferredSdpFmtpLine ||
        this._preferredSendAudioBandwidth !== newSettings.preferredSendAudioBandwidth ||
        this._preferredSendVideoBandwidth !== newSettings.preferredSendVideoBandwidth ||
        this._preferredReceiveAudioBandwidth !== newSettings.preferredReceiveAudioBandwidth ||
        this._preferredReceiveVideoBandwidth !== newSettings.preferredReceiveVideoBandwidth)
    );
  }

  public renegotiate(easyrtcId: string = null): void {
    if (easyrtcId) {
      this.rtc.renegotiate(easyrtcId);
    } else {
      if (this._isConnectedToRoom.getValue()) {
        this.room.getUsersWithoutMe().forEach((user) => {
          if(!user.peerconnection) {
            // only renegotiate with users having an active connection with
            return;
          }
          this.rtc.renegotiate(user.easyrtcid);
          console.log('Renegotiating with user ', user);
        });
      }
    }
  }

  public async initFileStream() {
    if (this._streamFromFile) {
      this.addCustomMediaStream(await this.getMediaStreamFromFile(this._fileToStream));
    } else {
      console.log('File streaming is not yet enabled');
    }
  }
  public async updateStreamInCall() {
    console.log('Updating streams');
    this.closeLocalMedia();
    this.renegotiate();
    await RetryablePromise.wait(500);
    if (this._streamFromFile) {
      await this.initFileStream();
    } else if (this._localAudioEnabled || this._localVideoEnabled) {
      try {
        await this.getLocalMedia();
      }
      catch(ex) {
        // noop
      }      
    }

    if (this._isConnectedToRoom.getValue()) {
      this.room.getUsersWithoutMe().forEach(async (user) => {
        if(!user.peerconnection) {
          // we are connected to a room but have not yer established the call
          return;
        }
        const retry = 10;
        let current = 0;
        while (user.peerconnection.signalingState !== 'stable' && current < retry) {
          console.log('Waiting for the peerconnection to become stable before adding stream');
          await RetryablePromise.wait(250);
          current++;
        }
        this.rtc.addStreamToCall(user.easyrtcid, 'default', null);
      });
    }
  }

  private getMediaStreamFromFile(file: File): Promise<MediaStream> {
    return new Promise<MediaStream>((resolve, reject) => {
      this._shadowVideoElement = document.createElement('video');
      this._shadowVideoElement.muted = true;
      this._shadowVideoElement.loop = true;

      this._shadowVideoElement.oncanplaythrough = () => {
        try {
          let captureStream: () => MediaStream =
            (this._shadowVideoElement as any)?.captureStream || (this._shadowVideoElement as any)?.mozCaptureStream;

          const fileMediaStream: MediaStream = captureStream.call(this._shadowVideoElement);

          // This is needed to make audio work with Firefox. It also fixes Audio sporadically slowing down/accellerating  or becoming out of sync with the video on Chrome.
          const audioTracks = EasyRtcService.getDecoupledAudioTracks(this._shadowVideoElement);
          EasyRtcService.replaceAudioTracks(fileMediaStream, audioTracks);
          resolve(fileMediaStream);
        } catch (e) {
          reject(e);
        } finally {
          this._shadowVideoElement.oncanplaythrough = null;
        }
      };

      try {
        const url = URL.createObjectURL(file);

        this._shadowVideoElement.src = url;
      } catch (e) {
        reject(e);
      }
    });
  }

  private static getDecoupledAudioTracks(element: HTMLMediaElement): MediaStreamTrack[] {
    const ctx = new AudioContext();
    const dest = ctx.createMediaStreamDestination();
    const sourceNode = ctx.createMediaElementSource(element);

    sourceNode.connect(dest);

    element.muted = false;

    return dest.stream.getAudioTracks();
  }

  private static replaceAudioTracks(stream: MediaStream, tracks: MediaStreamTrack[]) {
    stream.getAudioTracks().forEach((track) => stream.removeTrack(track));

    tracks.forEach((track) => stream.addTrack(track));
  }

  public addCustomMediaStream(stream: MediaStream) {
    this.rtc.register3rdPartyLocalMediaStream(stream, 'default');
    this.localStream = stream;
    this.room?.getMe()?.addStream(stream);
  }

  public playFileStream() {
    this?._shadowVideoElement?.play();
  }

  public pauseFileStream() {
    this?._shadowVideoElement?.pause();
  }

  public isFileStreamPaused(): boolean {
    return this?._shadowVideoElement?.paused;
  }

  public replayFileStream() {
    if (this?._shadowVideoElement?.currentTime) {
      this._shadowVideoElement.currentTime = 0;
    }
  }

  private cleanup() {
    this._isReadyToCall.next(false);
    this._isConnectedToRoom.next(false);
    this._connectionWithAtLeastOneUserEstablished.next(false);
    this._room = undefined;
    this.rtc.hangupAll();
    this.closeLocalMedia();
    this.rtc.disconnect();
    this.connectSocket();
    if (this._shadowVideoElement) {
      this._shadowVideoElement.src = null;
      this._shadowVideoElement = undefined;
    }
  }

  public onEvent(event: Event): Observable<any> {
    return new Observable<Event>((observer) => {
      this.socket.on(event, (param) => {
        console.log('Event: ', event);
        observer.next(param);
      });
    });
  }

  ngOnDestroy() {
    this.cleanup();
    this.onDestroy$.next(true);
    this.onDestroy$.complete();
  }
}
