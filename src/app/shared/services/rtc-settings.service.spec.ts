import { TestBed } from '@angular/core/testing';

import { RtcSettingsService } from './rtc-settings.service';

describe('RtcSettingsService', () => {
  let service: RtcSettingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RtcSettingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
