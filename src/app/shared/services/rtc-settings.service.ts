import { Injectable } from '@angular/core';
import { RtcSettings } from '../model/rtc-settings';
import { EasyRtcService } from './easy-rtc.service';
import { RtcCodecs, CodecConfig } from '../rtc-media-settings/rtc-codecs';

@Injectable({
  providedIn: 'root',
})
export class RtcSettingsService {
  public settings: RtcSettings = {};

  private _availableSendAudioCodecs: CodecConfig[];
  private _availableSendVideoCodecs: CodecConfig[];
  private _availableReceiveAudioCodecs: CodecConfig[];
  private _availableReceiveVideoCodecs: CodecConfig[];

  public get needsLocalMedia() {
    return this.settings.streamFile ? false : this.settings?.sendAudio || this?.settings.sendVideo;
  }

  /**
   * Getter availableSendAudioCodecs
   * @return {CodecConfig[]}
   */
  public get availableSendAudioCodecs(): CodecConfig[] {
    return this._availableSendAudioCodecs;
  }

  /**
   * Getter availableSendVideoCodecs
   * @return {CodecConfig[]}
   */
  public get availableSendVideoCodecs(): CodecConfig[] {
    return this._availableSendVideoCodecs;
  }

  /**
   * Getter availableReceiveAudioCodecs
   * @return {CodecConfig[]}
   */
  public get availableReceiveAudioCodecs(): CodecConfig[] {
    return this._availableReceiveAudioCodecs;
  }

  /**
   * Getter availableReceiveVideoCodecs
   * @return {CodecConfig[]}
   */
  public get availableReceiveVideoCodecs(): CodecConfig[] {
    return this._availableReceiveVideoCodecs;
  }

  constructor(private rtc: EasyRtcService) {
    this.loadDefaultSettings();
    this.updateSupportedCodecs();
  }

  // This is needed because each call of getSupported*CodecsAsCodecConfig, returns new references for the codecs. This makes it hard to keep track of the current settings and makes Angular's change detection misbehave. This will cache the existing references and update them only, if the caps really change
  public updateSupportedCodecs(): void {
    this._availableSendAudioCodecs = RtcSettingsService.updateCodecsIfNecessary(
      this._availableSendAudioCodecs,
      RtcCodecs.getSupportedSendCodecsAsCodecConfig('audio')
    );
    this._availableSendVideoCodecs = RtcSettingsService.updateCodecsIfNecessary(
      this._availableSendVideoCodecs,
      RtcCodecs.getSupportedSendCodecsAsCodecConfig('video')
    );
    this._availableReceiveAudioCodecs = RtcSettingsService.updateCodecsIfNecessary(
      this._availableReceiveAudioCodecs,
      RtcCodecs.getSupportedReceiveCodecsAsCodecConfig('audio')
    );
    this._availableReceiveVideoCodecs = RtcSettingsService.updateCodecsIfNecessary(
      this._availableReceiveVideoCodecs,
      RtcCodecs.getSupportedReceiveCodecsAsCodecConfig('video')
    );
  }

  private static updateCodecsIfNecessary(previousCodecs: CodecConfig[], newCodecs: CodecConfig[]): CodecConfig[] {
    const updated = new Array<CodecConfig>();
    if (!previousCodecs) {
      return newCodecs;
    }
    newCodecs.forEach((codec) => {
      const previousRef = previousCodecs.find((prev) => RtcCodecs.codecCapabilitiesEquals(prev.codecCapability, codec.codecCapability));
      if (previousRef) {
        updated.push(previousRef);
      } else {
        updated.push(codec);
        console.log('new Codec is available');
      }
    });

    return updated;
  }

  private loadDefaultSettings() {
    this.settings.localIceCandidateFilter = this.rtc.localCandidateFilter.toString();
    this.settings.remoteIceCandidateFilter = this.rtc.remoteCandidateFilter.toString();
    this.settings.sendAudio = this.rtc.localAudioEnabled;
    this.settings.sendVideo = this.rtc.localVideoEnabled;
    this.settings.autoplayOnFirstParticipant = this.rtc.autoplayStreamOnFirstParticipant;
  }

  public async applySettings() {
    await this.rtc.applySettings(this.settings);
    console.log(this.settings);
  }
}
