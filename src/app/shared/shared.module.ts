import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { WebsocketConnectionSnackbarComponent } from './websocket-connection-snackbar/websocket-connection-snackbar.component';
import { TransportComponent } from './peer-stats/transport/transport.component';
import { NgxFilesizeModule } from 'ngx-filesize';
import { CommonStatComponent } from './peer-stats/common-stat/common-stat.component';
import { StatPropertyComponent } from './peer-stats/stat-property/stat-property.component';
import { CandidateStatsComponent } from './peer-stats/candidate-stats/candidate-stats.component';
import { CandidatePairStatsComponent } from './peer-stats/candidate-pair-stats/candidate-pair-stats.component';
import { StatProprtiesDisplayComponent } from './peer-stats/stat-properties-display/stat-properties-display.component';
import { OutboundRtpStatsComponent } from './peer-stats/outbound-rtp-stats/outbound-rtp-stats.component';
import { CertificateStatsComponent } from './peer-stats/certificate-stats/certificate-stats.component';
import { CodecStatsComponent } from './peer-stats/codec-stats/codec-stats.component';
import { TrackStatsComponent } from './peer-stats/track-stats/track-stats.component';
import { RemoteInboundRtpStatsComponent } from './peer-stats/remote-inbound-rtp-stats/remote-inbound-rtp-stats.component';
import { MediaSourceStatsComponent } from './peer-stats/media-source-stats/media-source-stats.component';
import { InboundRtpStatsComponent } from './peer-stats/inbound-rtp-stats/inbound-rtp-stats.component';
import { RemoteOutboundRtpStatsComponent } from './peer-stats/remote-outbound-rtp-stats/remote-outbound-rtp-stats.component';
import { SummaryStatsComponent } from './peer-stats/summary-stats/summary-stats.component';
import { SummaryStatPropertyComponent } from './peer-stats/summary-stat-property/summary-stat-property.component';
import { StatPropertyValueRendererComponent } from './peer-stats/stat-property-value-renderer/stat-property-value-renderer.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SdpRendererComponent } from './peer-stats/sdp-renderer/sdp-renderer.component';
import { IsValidRegularExpressionDirective } from './directives/is-valid-regular-expression.directive';
import { RequiredIfDirective } from './directives/required-if.directive';
import { FileInputValueAccessorDirective } from './directives/file-input-value-accessor.directive';
import { ApiLogButtonComponent } from './api-log-button/api-log-button.component';

@NgModule({
  imports: [CommonModule, MaterialModule, NgxFilesizeModule, FlexLayoutModule],
  exports: [
    MaterialModule,
    TransportComponent,
    CandidatePairStatsComponent,
    CandidateStatsComponent,
    OutboundRtpStatsComponent,
    CertificateStatsComponent,
    CodecStatsComponent,
    TrackStatsComponent,
    RemoteInboundRtpStatsComponent,
    MediaSourceStatsComponent,
    InboundRtpStatsComponent,
    RemoteOutboundRtpStatsComponent,
    SummaryStatsComponent,
    SdpRendererComponent,
    IsValidRegularExpressionDirective,
    RequiredIfDirective,
    FileInputValueAccessorDirective,
    ApiLogButtonComponent
  ],
  declarations: [
    WebsocketConnectionSnackbarComponent,
    TransportComponent,
    CommonStatComponent,
    StatPropertyComponent,
    CandidateStatsComponent,
    CandidatePairStatsComponent,
    StatProprtiesDisplayComponent,
    OutboundRtpStatsComponent,
    CertificateStatsComponent,
    CodecStatsComponent,
    TrackStatsComponent,
    RemoteInboundRtpStatsComponent,
    MediaSourceStatsComponent,
    InboundRtpStatsComponent,
    RemoteOutboundRtpStatsComponent,
    SummaryStatsComponent,
    SummaryStatPropertyComponent,
    StatPropertyValueRendererComponent,
    SdpRendererComponent,
    IsValidRegularExpressionDirective,
    RequiredIfDirective,
    FileInputValueAccessorDirective,
    ApiLogButtonComponent
  ],
  entryComponents: [WebsocketConnectionSnackbarComponent],
})
export class SharedModule {}
