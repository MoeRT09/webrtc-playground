export class Util {
  public static getArrayAExceptB<T>(A: Array<T>, B: Array<T>): Array<T> {
    return A.filter((value) => !B.includes(value));
  }
  public static getArrayAIntersectB<T>(A: Array<T>, B: Array<T>): Array<T> {
    return A.filter((value) => B.includes(value));
  }

  public static removeElementFromArray<T>(elementToRemove: T, targetArray: Array<T>): Array<T> {
    const indexOfElement = targetArray.indexOf(elementToRemove);
    if (indexOfElement > -1) {
      targetArray.splice(indexOfElement, 1);
    }
    return targetArray;
  }

  public static getArrayIntersections<T>(...arrays: Array<Array<T>>) {
    return arrays.reduce((arrayWithCommonProps: Array<T>, currentArray: Array<T>) => {
      return Util.getArrayAIntersectB(arrayWithCommonProps, currentArray);
    });
  }

  public static getArrayAExceptRest<T>(A: Array<T>, ...arrays: Array<Array<T>>) {
    return arrays.reduce((arrayWithUniqueProps: Array<T>, currentArray: Array<T>) => {
      return Util.getArrayAExceptB(arrayWithUniqueProps, currentArray);
    }, A);
  }

  public static getDistinctArray<T>(arrayWithDuplicates: Array<T>) {
    return Array.from(new Set<T>(arrayWithDuplicates));
  }

  public static mapToObject<T>(map: Map<string, T>): object {
    const result = {};
    map.forEach((value, key) => {
      result[key] = value;
    });
    return result;
  }
}
