import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebsocketConnectionSnackbarComponent } from './websocket-connection-snackbar.component';

describe('WebsocketConnectionSnackbarComponent', () => {
  let component: WebsocketConnectionSnackbarComponent;
  let fixture: ComponentFixture<WebsocketConnectionSnackbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebsocketConnectionSnackbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebsocketConnectionSnackbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
