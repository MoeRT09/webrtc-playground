import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
  selector: 'wp-websocket-connection-snackbar',
  templateUrl: './websocket-connection-snackbar.component.html',
  styleUrls: ['websocket-connection-snackbar.css']
})
export class WebsocketConnectionSnackbarComponent {

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: {webSocketServer: string}) {}

}
