export const environment = {
  production: true,
  socketUrl: "https://my-signalling-server.example.com"
};
